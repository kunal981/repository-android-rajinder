package com.brst.alcohol.toxicology.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.util.Log;

public class TimeUtil {

	public static String TAG = "TimeUtil";

	public static boolean isTestTimeSet = false;
	public static int hourUnitTestTime = 0;
	public static int minUnitTestTime = 0;
	public static int hourUnitIncident = 0;
	public static int minUnitIncident = 0;

	public static boolean isTestTimeValid() {
		boolean status = true;

		if (hourUnitTestTime == hourUnitIncident) {

			if (minUnitTestTime <= minUnitIncident) {
				status = false;
			}

		} else if (hourUnitTestTime < hourUnitIncident) {
			status = false;
		}
		return status;

	}

	public static int getMinuteDifferene() {

		int minDif;
		minDif = (hourUnitTestTime - hourUnitIncident)
				* 60
				+ (minUnitTestTime >= minUnitIncident ? minUnitTestTime
						- minUnitIncident : (minUnitTestTime - minUnitIncident));

		return minDif;

	}

	public static void initTimeUnit() {
		isTestTimeSet = false;
		hourUnitTestTime = 0;
		minUnitTestTime = 0;
		hourUnitIncident = 0;
		minUnitIncident = 0;

	}

	public static String getCurrentDate() {
		String currentDate = null;
		Calendar c = Calendar.getInstance();

		Log.i(TAG, "Current date :" + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy",
				java.util.Locale.getDefault());
		currentDate = df.format(c.getTime());
		Log.i(TAG, "Formated date :" + currentDate);
		return currentDate;
	}

}
