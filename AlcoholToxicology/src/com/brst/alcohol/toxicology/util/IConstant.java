package com.brst.alcohol.toxicology.util;

public class IConstant {

	public static final String DRINK_1 = "Wine";
	public static final String DRINK_2 = "Bear";
	public static final String DRINK_3 = "Malt";
	public static final String DRINK_4 = "80-Poof";
	public static final String DRINK_5 = "custom_drink";

	public static final String KEY_ONE = "one";
	public static final String KEY_TWO = "two";
	public static final String KEY_THREE = "three";
	public static final String KEY_FOUR = "four";
	public static final String KEY_FIVE = "five";
	public static final String KEY_SIX = "six";

}
