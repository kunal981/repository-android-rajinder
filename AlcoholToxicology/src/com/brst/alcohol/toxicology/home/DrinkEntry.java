package com.brst.alcohol.toxicology.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.modal.Drink;
import com.brst.alcohol.toxicology.util.IConstant;
import com.brst.alcohol.toxicology.util.TimeUtil;

public class DrinkEntry extends Fragment implements OnClickListener,
		OnItemSelectedListener {

	public static final int REQUEST_CODE = 328;

	Button btnCalculate;
	ImageView drink_1, drink_2, drink_3, drink_4, drink_5;
	// ImageButton btnAdd;
	ListView listView;
	TextView txtCaseName;

	Spinner spinnerDuration;

	CaseDataAdapter cData;
	CaseProfileModal cCaseData;

	HashMap<String, Drink> initDataMap;

	HashMap<String, Integer> drinkQuantity;
	int d1, d2, d3, d4, d5;
	List<Drink> listDrink;
	CustomListDrinkAdapter adapter;
	double drinkDuration = 0.0;

	List<Drink> items;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cData = CaseDataAdapter.getInstance();
		cCaseData = CaseDataAdapter.getInstance().getCaseProfile();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_drink_entry_2,
				container, false);

		drinkQuantity = new HashMap<String, Integer>();

		initData();

		listView = (ListView) rootView.findViewById(R.id.list);
		View footer = getActivity().getLayoutInflater().inflate(
				R.layout.layout_list_drink_footer, null);
		listView.addFooterView(footer);

		txtCaseName = (TextView) rootView.findViewById(R.id.text_case_id);
		btnCalculate = (Button) rootView.findViewById(R.id.btn_calulate);
		spinnerDuration = (Spinner) rootView
				.findViewById(R.id.spinner_duration);
		btnCalculate.setOnClickListener(this);
		spinnerDuration.setOnItemSelectedListener(this);

		txtCaseName.setText(cCaseData.getDrinkerName());

		listDrink = new ArrayList<Drink>();
		adapter = new CustomListDrinkAdapter(getActivity(), listDrink);
		listView.setAdapter(adapter);

		drink_1 = (ImageView) rootView.findViewById(R.id.drink_1);
		drink_2 = (ImageView) rootView.findViewById(R.id.drink_2);
		drink_3 = (ImageView) rootView.findViewById(R.id.drink_3);
		drink_4 = (ImageView) rootView.findViewById(R.id.drink_4);
		drink_5 = (ImageView) rootView.findViewById(R.id.drink_5);

		drink_1.setImageResource(R.drawable.drink_1_selector);
		drink_2.setImageResource(R.drawable.drink_2_selector);
		drink_3.setImageResource(R.drawable.drink_3_selector);
		drink_4.setImageResource(R.drawable.drink_4_selector);
		drink_5.setImageResource(R.drawable.drink_5_selector);

		drink_1.setOnClickListener(this);
		drink_2.setOnClickListener(this);
		drink_3.setOnClickListener(this);
		drink_4.setOnClickListener(this);
		drink_5.setOnClickListener(this);

		return rootView;
	}

	private void initData() {

		d1 = d2 = d3 = d4 = d5 = 0;
		Drink d1 = new Drink(IConstant.DRINK_1,
				getString(R.string.text_drink_wine), 12.0, 5.0);
		Drink d2 = new Drink(IConstant.DRINK_2,
				getString(R.string.text_drink_bear), 5.0, 12.0);
		Drink d3 = new Drink(IConstant.DRINK_3,
				getString(R.string.text_drink_malt), 7.0, 9.0);
		Drink d4 = new Drink(IConstant.DRINK_4,
				getString(R.string.text_drink_80_poof), 40.0, 1.5);

		initDataMap = cData.getInitDrinkData();
		if (initDataMap == null) {
			initDataMap = new HashMap<String, Drink>();
			initDataMap.put(IConstant.DRINK_1, d1);
			initDataMap.put(IConstant.DRINK_2, d2);
			initDataMap.put(IConstant.DRINK_3, d3);
			initDataMap.put(IConstant.DRINK_4, d4);
			cData.setInitDrinkData(initDataMap);
		}

	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_calulate:
			if (adapter.getCount() != 0) {
				cCaseData = cData.getCaseProfile();
				if (cCaseData != null) {
					cCaseData.setCreateOn(TimeUtil.getCurrentDate());
					cCaseData.setDuration(drinkDuration);
					cCaseData.setDrinksInfo(drinkQuantity);
					cData.setCaseProfile(cCaseData);
					spinnerDuration.setSelection(0);
					drinkQuantity = new HashMap<String, Integer>();
					Result fragment = new Result();
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(fragment, true);

				} else {
					Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(getActivity(), "Select alteast one Drink",
						Toast.LENGTH_LONG).show();
			}

			break;

		case R.id.drink_1:
			d1++;
			drinkQuantity.put(IConstant.DRINK_1, d1);
			adapter.addItem(initDataMap.get(IConstant.DRINK_1));
			drink_1.setImageResource(R.drawable.drink_1_selected);

			break;
		case R.id.drink_2:
			d2++;
			drinkQuantity.put(IConstant.DRINK_2, d2);
			adapter.addItem(initDataMap.get(IConstant.DRINK_2));
			drink_2.setImageResource(R.drawable.drink_2_selected);
			break;
		case R.id.drink_3:
			d3++;
			drinkQuantity.put(IConstant.DRINK_3, d3);
			adapter.addItem(initDataMap.get(IConstant.DRINK_3));
			drink_3.setImageResource(R.drawable.drink_3_selected);
			break;
		case R.id.drink_4:
			d4++;
			drinkQuantity.put(IConstant.DRINK_4, d4);
			adapter.addItem(initDataMap.get(IConstant.DRINK_4));
			drink_4.setImageResource(R.drawable.drink_4_selected);
			break;
		case R.id.drink_5:
			// d5++;
			// drinkQuantity.put(IConstant.DRINK_5, d5);
			// adapter.addItem(initDataMap.get(IConstant.DRINK_5));
			// drink_5.setImageResource(R.drawable.drink_5_selected);
			// Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_LONG)
			// .show();
			Intent intent = new Intent(getActivity(),
					CustomDrinkDailogActivity.class);
			startActivityForResult(intent, REQUEST_CODE);
			// showDialog();
			break;

		default:
			break;
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		intent.putExtra("requestCode", requestCode);
		super.startActivityForResult(intent, requestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (data != null) {
			if (resultCode == -1) {
				if (data.getExtras().get("requestCode").equals(REQUEST_CODE)) {

					Drink drink = data.getParcelableExtra("item");
					initDataMap = cData.getInitDrinkData();
					initDataMap.put(drink.getId() + "", drink);
					cData.setInitDrinkData(initDataMap);
					d5++;
					drinkQuantity.put(drink.getId() + "", 1);
					adapter.addItem(drink);
					drink_5.setImageResource(R.drawable.drink_5_selected);
				}
			}
		}

	}

	// Background task to retrive custom Drink list from database

	class CustomListDrinkAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		private List<Drink> listDrinks;

		public CustomListDrinkAdapter(Activity activity, List<Drink> listDrinks) {
			this.activity = activity;
			this.listDrinks = listDrinks;
		}

		@Override
		public int getCount() {
			return listDrinks.size();
		}

		@Override
		public Object getItem(int location) {
			return listDrinks.get(location);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void addItem(Drink drink) {
			listDrinks.add(drink);
			notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(R.layout.layout_list_row, null);

			EditText drinkName = (EditText) convertView
					.findViewById(R.id.id_text_drink);
			EditText drinkDesc = (EditText) convertView
					.findViewById(R.id.id_text_drink_value);

			ImageView btn_cancel = (ImageView) convertView
					.findViewById(R.id.img_text_cancel);

			// getting movie data for the row
			final Drink item = listDrinks.get(position);
			drinkName.setText(item.getName().toString());
			String percent = item.getPercentAlcohol() + "";
			String volume = item.getVolumeAlcohol() + "";
			StringBuilder str = new StringBuilder();
			str.append(percent).append("% ").append(volume).append(" fl.oz");
			drinkDesc.setText(str.toString());

			btn_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					Drink drink = listDrinks.get(position);

					String key = drink.getDrinkType();
					initDataMap.remove(drink.getId() + "");
					cData.setInitDrinkData(initDataMap);
					if (key.equals(IConstant.DRINK_5)) {
						int id = drinkQuantity.get(drink.getId() + "");
						d5 = d5 - id;
						if (d5 < 1) {
							drink_5.setImageResource(R.drawable.drink_5);
						}
						drinkQuantity.remove(drink.getId());
					} else {
						removeDrink(key);
					}
					listDrinks.remove(position);
					notifyDataSetChanged();

				}

			});

			return convertView;
		}

	}

	private void removeDrink(String key) {
		int id = 0;
		switch (key) {
		case IConstant.DRINK_1:
			id = drinkQuantity.get(key);
			if (id == 1) {
				drink_1.setImageResource(R.drawable.drink_1);
			}
			id = id - 1;
			d1 = id;
			drinkQuantity.put(key, d1);
			break;
		case IConstant.DRINK_2:
			id = drinkQuantity.get(key);
			if (id == 1) {
				drink_2.setImageResource(R.drawable.drink_2);
			}
			id = id - 1;
			d2 = id;
			drinkQuantity.put(key, d2);
			break;
		case IConstant.DRINK_3:
			id = drinkQuantity.get(key);
			if (id == 1) {
				drink_3.setImageResource(R.drawable.drink_3);
			}
			id = id - 1;
			d3 = id;
			drinkQuantity.put(key, d3);
			break;
		case IConstant.DRINK_4:
			id = drinkQuantity.get(key);
			if (id == 1) {
				drink_4.setImageResource(R.drawable.drink_4);
			}
			id = id - 1;
			d4 = id;
			drinkQuantity.put(key, d4);
			break;
		// case IConstant.DRINK_5:
		// id = drinkQuantity.get(key);
		// if (id == 1) {
		// drink_5.setImageResource(R.drawable.drink_5);
		// }
		// id = id - 1;
		// d5 = id;
		// drinkQuantity.put(key, d5);
		// break;

		default:
			break;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long arg3) {

		String str = parent.getItemAtPosition(position).toString();
		String value = (str.split(" "))[0];
		drinkDuration = Double.parseDouble(value);

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	class CustomDrinkAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		private List<Drink> listDrinks;

		public CustomDrinkAdapter(Activity activity, List<Drink> listDrinks) {
			super();
			this.activity = activity;
			this.listDrinks = listDrinks;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listDrinks.size();
		}

		@Override
		public Object getItem(int location) {
			// TODO Auto-generated method stub
			return listDrinks.get(location);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public void updateItem(List<Drink> listDrinks) {
			this.listDrinks = listDrinks;
			notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_list_row, null);

				holder = new ViewHolder();
				holder.drinkName = (EditText) convertView
						.findViewById(R.id.id_text_drink);
				holder.drinkDesc = (EditText) convertView
						.findViewById(R.id.id_text_drink_value);

				holder.iconAdd = (ImageView) convertView
						.findViewById(R.id.img_text_cancel);
				holder.iconAdd.setImageResource(R.drawable.btn_add);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final Drink item = listDrinks.get(position);
			holder.drinkName.setText(item.getName().toString());
			String percent = item.getPercentAlcohol() + "";
			String volume = item.getVolumeAlcohol() + "";
			StringBuilder str = new StringBuilder();
			str.append(percent).append("% ").append(volume).append(" fl.oz");
			holder.drinkDesc.setText(str.toString());

			holder.iconAdd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					// Toast.makeText(CustomDrinkDailogActivity.this,
					// "Coming soon", Toast.LENGTH_SHORT).show();
					// setResult(gRESULT_OK);
					// finish();
				}

			});
			return convertView;
		}

	}

	static class ViewHolder {
		EditText drinkName;
		EditText drinkDesc;
		ImageView iconAdd;
	}

}
