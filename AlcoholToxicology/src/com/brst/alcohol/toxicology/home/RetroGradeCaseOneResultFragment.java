package com.brst.alcohol.toxicology.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.modal.CaseRetrogradeProfileModel;

public class RetroGradeCaseOneResultFragment extends Fragment implements
		OnClickListener {

	CaseDataAdapter cAdapter;

	TextView txtCasename, textHead;
	TextView title;

	CaseRetrogradeProfileModel caseRetrogradeProfileModel;
	private static String TAG = "RetroGradeCaseOneResultFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragement_layout_retro_case_one, null);
		cAdapter = CaseDataAdapter.getInstance();
		caseRetrogradeProfileModel = cAdapter.getCaseRetroGradeModel();

		txtCasename = (TextView) rootView.findViewById(R.id.id_case_name);
		textHead = (TextView) rootView
				.findViewById(R.id.text_retrograde_case_one_text1);
		txtCasename.setText(caseRetrogradeProfileModel.getCaseName());

		StringBuilder textTitle = new StringBuilder();
		textTitle.append("I.E. ")
				.append(caseRetrogradeProfileModel.getIncidentTime())
				.append(" (Incident) - ")
				.append(caseRetrogradeProfileModel.getTestTime())
				.append(" BAC test");
		textHead.setText(textTitle.toString());

		title = (TextView) rootView.findViewById(R.id.header_title);
		title.setText(getString(R.string.header_title_alcohol_toxicology_result));

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			Log.e(TAG, savedInstanceState.getString("message"));
		}
		Log.e(TAG, "onActivityCreated");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			Log.e(TAG, savedInstanceState.getString("message"));
		}
		Log.e(TAG, "onCreate");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e(TAG, "onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.e(TAG, "onPause");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.e(TAG, "onSaveInstanceState");
		outState.putString("message", "This is my message to be reloaded");
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {

		default:
			break;
		}

	}

}
