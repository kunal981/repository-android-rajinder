package com.brst.alcohol.toxicology;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {

	private int SPLASH_TIME_OUT = 1200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				Intent intent_home = new Intent(SplashActivity.this,
						MainActivity.class);
				startActivity(intent_home);
				finish();
			}
		}, SPLASH_TIME_OUT);

	}

}
