package com.brst.alcohol.toxicology.modal;

import java.util.HashMap;

public class CaseProfileModal {

	private long caseId = -1;
	private String caseMethod;
	private String createOn;
	private String bacValue;

	private String drinkerName;
	private String drinkderGender;
	private String drinkderWeight;
	private HashMap<String, Integer> drinksInfo;

	private double duration;

	public long getCaseId() {
		return caseId;
	}

	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}

	public String getBacValue() {
		return bacValue;
	}

	public void setBacValue(String bacValue) {
		this.bacValue = bacValue;
	}

	public String getCaseMethod() {
		return caseMethod;
	}

	public String getCreateOn() {
		return createOn;
	}

	public void setCreateOn(String createOn) {
		this.createOn = createOn;
	}

	public void setCaseMethod(String caseMethod) {
		this.caseMethod = caseMethod;
	}

	public String getDrinkerName() {
		return drinkerName;
	}

	public void setDrinkerName(String drinkerName) {
		this.drinkerName = drinkerName;
	}

	public String getDrinkderGender() {
		return drinkderGender;
	}

	public void setDrinkderGender(String drinkderGender) {
		this.drinkderGender = drinkderGender;
	}

	public String getDrinkderWeight() {
		return drinkderWeight;
	}

	public void setDrinkderWeight(String drinkderWeight) {
		this.drinkderWeight = drinkderWeight;
	}

	public HashMap<String, Integer> getDrinksInfo() {
		return drinksInfo;
	}

	public void setDrinksInfo(HashMap<String, Integer> drinksInfo) {
		this.drinksInfo = drinksInfo;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

}
