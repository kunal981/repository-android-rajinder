package com.brst.alcohol.toxicology.modal;

public class Facts {

	private String question;
	private String answer;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Facts(String question, String answer) {
		super();
		this.question = question;
		this.answer = answer;
	}

}
