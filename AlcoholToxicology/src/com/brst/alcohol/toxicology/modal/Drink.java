package com.brst.alcohol.toxicology.modal;

import android.os.Parcel;
import android.os.Parcelable;

public class Drink implements Parcelable {

	private long id;
	private String drinkType;
	private String name;
	private double percentAlcohol;
	private double volumeAlcohol;

	public Drink() {

	}

	public Drink(String drinkType, String name, double percentAlcohol,
			double volumeAlcohol) {
		super();
		this.drinkType = drinkType;
		this.name = name;
		this.percentAlcohol = percentAlcohol;
		this.volumeAlcohol = volumeAlcohol;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPercentAlcohol() {
		return percentAlcohol;
	}

	public void setPercentAlcohol(double percentAlcohol) {
		this.percentAlcohol = percentAlcohol;
	}

	public double getVolumeAlcohol() {
		return volumeAlcohol;
	}

	public void setVolumeAlcohol(double volumeAlcohol) {
		this.volumeAlcohol = volumeAlcohol;
	}

	public String getDrinkType() {
		return drinkType;
	}

	public void setDrinkType(String drinkType) {
		this.drinkType = drinkType;
	}

	Drink(Parcel in) {
		this.id = in.readLong();
		this.drinkType = in.readString();
		this.name = in.readString();
		this.percentAlcohol = in.readDouble();
		this.volumeAlcohol = in.readDouble();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flag) {
		dest.writeLong(id);
		dest.writeString(drinkType);
		dest.writeString(name);
		dest.writeDouble(percentAlcohol);
		dest.writeDouble(volumeAlcohol);

	}

	public static final Parcelable.Creator<Drink> CREATOR = new Parcelable.Creator<Drink>() {

		public Drink createFromParcel(Parcel in) {
			return new Drink(in);
		}

		public Drink[] newArray(int size) {
			return new Drink[size];
		}
	};

}
