package com.brst.alcohol.toxicology.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.modal.Facts;

public class FactsFragment extends Fragment {

	private List<String> parentItems;
	private List<String> childItems;
	ArrayList<Facts> listItems;

	ExpandableListView expandableListView;
	MyExpandableAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setGroupParents();
		// Set The Child Data
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_fact, null);
		expandableListView = (ExpandableListView) rootView
				.findViewById(R.id.list_exp);
		setUpGroupIndicator();

		adapter = new MyExpandableAdapter(getActivity(), listItems);
		expandableListView.setAdapter(adapter);

		return rootView;
	}

	public class MyExpandableAdapter extends BaseExpandableListAdapter {

		private Context context;
		private ArrayList<Facts> listItems;

		// constructor
		public MyExpandableAdapter(Context context, ArrayList<Facts> listItems) {
			this.context = context;
			this.listItems = listItems;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Object getChild(int groupPosition, int childPosition) {
			String child = listItems.get(groupPosition).getAnswer();
			return child;

		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {

			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.layout_expandable_child_item, null);
			}
			String child = listItems.get(groupPosition).getAnswer();
			TextView txt = (TextView) convertView
					.findViewById(R.id.text_child_id);

			txt.setText(child);

			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Object getGroup(int groupPosition) {

			return listItems.get(groupPosition);
		}

		@Override
		public int getGroupCount() {

			return listItems.size();
		}

		@Override
		public long getGroupId(int groupPosition) {

			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.layout_expandable_group_item_bg, null);
			}
			String questions = listItems.get(groupPosition).getQuestion();

			TextView txt = (TextView) convertView
					.findViewById(R.id.text_group_id);
			txt.setText(questions);

			return convertView;
		}

		@Override
		public boolean hasStableIds() {

			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {

			return false;
		}
	}

	@SuppressLint("NewApi")
	private void setUpGroupIndicator() {
		DisplayMetrics metrics;
		int width;
		metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay()
				.getMetrics(metrics);
		width = metrics.widthPixels;
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			expandableListView.setIndicatorBounds(width - GetPixelFromDips(35),
					width - GetPixelFromDips(5));

		} else {
			expandableListView.setIndicatorBoundsRelative(width
					- GetPixelFromDips(35), width - GetPixelFromDips(5));
		}
	}

	private int GetPixelFromDips(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	// private void setChildData() {
	// // Add Child Items for Fruits
	// childItems = new ArrayList<Object>();
	//
	// ArrayList<String> child = new ArrayList<String>();
	// child.add("");
	//
	// childItems.add(child);
	//
	// // Add Child Items for Flowers
	// child = new ArrayList<String>();
	// child.add("");
	//
	// childItems.add(child);
	//
	// // Add Child Items for Animals
	// child = new ArrayList<String>();
	// child.add("");
	//
	// childItems.add(child);
	//
	// // Add Child Items for Birds
	// child = new ArrayList<String>();
	// child.add("Data not available");
	//
	// childItems.add(child);
	//
	// }

	private void setGroupParents() {

		String[] questions = getResources().getStringArray(R.array.questions);
		String[] answers = getResources().getStringArray(R.array.answers);
		parentItems = Arrays.asList(questions);
		childItems = Arrays.asList(answers);

		listItems = new ArrayList<Facts>();
		for (int i = 0; i < parentItems.size(); i++) {
			Facts f1 = new Facts(parentItems.get(i), childItems.get(i));
			listItems.add(f1);
		}

	}

}
