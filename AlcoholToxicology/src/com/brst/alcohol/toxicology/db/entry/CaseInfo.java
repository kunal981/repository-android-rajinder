package com.brst.alcohol.toxicology.db.entry;

import android.provider.BaseColumns;

public class CaseInfo {

	public static abstract class CaseEntry implements BaseColumns {

		public static final String TABLE_NAME = "caseData";
		public static final String NAME = "cName";
		public static final String METHOD = "cMethod";
		public static final String BAC_VAL = "cBacVal";
		public static final String CREATED_ON = "cDate";
	}

}
