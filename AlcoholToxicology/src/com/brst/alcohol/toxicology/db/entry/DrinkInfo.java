package com.brst.alcohol.toxicology.db.entry;

import android.provider.BaseColumns;

public final class DrinkInfo {

	public DrinkInfo() {

	}

	public static abstract class DrinkTable implements BaseColumns {

		public static final String TABLE_NAME = "customdrink";
		public static final String COLUMN_DRINK_NAME = "drink_name";
		public static final String COLUMN_DRINK_TYPE = "drink_type";
		public static final String COLUMN_ALCOHOL_VOLUME = "alcohol_volume";
		public static final String COLUMN_ALCOHOL_PERCENTAGE = "alcohol_percentage";
	}

}
