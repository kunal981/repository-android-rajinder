package com.brst.alcohol.toxicology.db;

import java.sql.SQLException;
import java.util.ArrayList;

import com.brst.alcohol.toxicology.db.entry.CaseInfo.CaseEntry;
import com.brst.alcohol.toxicology.db.entry.DrinkInfo.DrinkTable;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.modal.Drink;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbOperation {

	private static String TAG = "DB_OPERATION";

	private SQLiteDatabase db;
	private DbHelper dbHelper;

	public DbOperation(Context context) {
		dbHelper = new DbHelper(context);
	}

	public void openWrite() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void openRead() throws SQLException {
		db = dbHelper.getReadableDatabase();
	}

	public void close() {
		db.close();
	}

	public boolean insertCustomDrink(Drink drink) throws SQLException {

		boolean status = false;
		openWrite();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DrinkTable.COLUMN_DRINK_TYPE, drink.getDrinkType());
		contentValues.put(DrinkTable.COLUMN_DRINK_NAME, drink.getName());
		contentValues.put(DrinkTable.COLUMN_ALCOHOL_PERCENTAGE,
				drink.getPercentAlcohol());
		contentValues.put(DrinkTable.COLUMN_ALCOHOL_VOLUME,
				drink.getVolumeAlcohol());

		long id = db.insert(DrinkTable.TABLE_NAME, null, contentValues);
		close();
		Log.i(TAG, id + "");
		if (id != 0) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}

	public long insertInsertCaseData(CaseProfileModal caseProfileModal) {
		long id = -1;
		try {
			openWrite();

			ContentValues contentValues = new ContentValues();
			contentValues
					.put(CaseEntry.NAME, caseProfileModal.getDrinkerName());
			contentValues.put(CaseEntry.METHOD,
					caseProfileModal.getCaseMethod());
			contentValues.put(CaseEntry.CREATED_ON,
					caseProfileModal.getCreateOn());
			contentValues
					.put(CaseEntry.BAC_VAL, caseProfileModal.getBacValue());
			if (caseProfileModal.getCaseId() == -1) {
				id = db.insert(CaseEntry.TABLE_NAME, null, contentValues);
			} else if (caseProfileModal.getCaseId() != -1
					&& isCaseExit(caseProfileModal.getCaseId())) {

				id = db.update(CaseEntry.TABLE_NAME, contentValues,
						CaseEntry._ID + " = ?", new String[] { String
								.valueOf(caseProfileModal.getCaseId()) });
			} else {
				id = db.insert(CaseEntry.TABLE_NAME, null, contentValues);
			}
			close();

			Log.e(TAG, id + "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}

	public ArrayList<Drink> getAllCustomDrink() throws SQLException {
		ArrayList<Drink> arraylistDrink = new ArrayList<Drink>();
		// hp = new HashMap();
		openRead();
		String[] projection = { DrinkTable._ID, DrinkTable.COLUMN_DRINK_TYPE,
				DrinkTable.COLUMN_DRINK_NAME,
				DrinkTable.COLUMN_ALCOHOL_PERCENTAGE,
				DrinkTable.COLUMN_ALCOHOL_VOLUME };

		String sortOrder = DrinkTable._ID + " DESC";

		Cursor cursor = db.query(DrinkTable.TABLE_NAME, // The table to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			Drink d = new Drink();

			d.setId(cursor.getLong(cursor.getColumnIndexOrThrow(DrinkTable._ID)));

			d.setDrinkType(cursor.getString(cursor
					.getColumnIndexOrThrow(DrinkTable.COLUMN_DRINK_TYPE)));
			d.setName(cursor.getString(cursor
					.getColumnIndexOrThrow(DrinkTable.COLUMN_DRINK_NAME)));
			d.setPercentAlcohol(Double.parseDouble(cursor.getString(cursor
					.getColumnIndexOrThrow(DrinkTable.COLUMN_ALCOHOL_PERCENTAGE))));
			d.setVolumeAlcohol(Double.parseDouble(cursor.getString(cursor
					.getColumnIndexOrThrow(DrinkTable.COLUMN_ALCOHOL_VOLUME))));
			arraylistDrink.add(d);
			cursor.moveToNext();

		}
		close();
		return arraylistDrink;
	}

	public ArrayList<CaseProfileModal> getAllCases() throws SQLException {
		ArrayList<CaseProfileModal> arraylistCases = new ArrayList<CaseProfileModal>();
		// hp = new HashMap();
		openRead();
		String[] projection = { CaseEntry._ID, CaseEntry.NAME,
				CaseEntry.METHOD, CaseEntry.CREATED_ON, CaseEntry.BAC_VAL };

		String sortOrder = DrinkTable._ID + " DESC";

		Cursor cursor = db.query(CaseEntry.TABLE_NAME, // The table to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			CaseProfileModal d = new CaseProfileModal();

			d.setCaseId(cursor.getLong(cursor
					.getColumnIndexOrThrow(CaseEntry._ID)));

			d.setDrinkerName(cursor.getString(cursor
					.getColumnIndexOrThrow(CaseEntry.NAME)));
			d.setCreateOn(cursor.getString(cursor
					.getColumnIndexOrThrow(CaseEntry.CREATED_ON)));
			d.setCaseMethod(cursor.getString(cursor
					.getColumnIndexOrThrow(CaseEntry.METHOD)));
			d.setBacValue(cursor.getString(cursor
					.getColumnIndexOrThrow(CaseEntry.BAC_VAL)));
			arraylistCases.add(d);
			cursor.moveToNext();

		}
		close();
		return arraylistCases;
	}

	public boolean isCaseExit(long id) throws SQLException {

		boolean status = false;
		openRead();
		String[] projection = { CaseEntry._ID, CaseEntry.NAME,
				CaseEntry.METHOD, CaseEntry.CREATED_ON, CaseEntry.BAC_VAL };

		Cursor cursor = db.query(CaseEntry.TABLE_NAME, // The table to query
				projection, // The columns to return
				CaseEntry._ID + "=?", // The columns for the WHERE clause
				new String[] { String.valueOf(id) }, // The values for the WHERE
														// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		if (cursor != null && cursor.isAfterLast()) {
			try {
				if (cursor.getString(cursor
						.getColumnIndexOrThrow(CaseEntry.NAME)) != null) {
					status = true;
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				status = false;
			}
		}
		return status;

	}

	public boolean deleteEntry(long id) throws SQLException {

		boolean status = false;
		// hp = new HashMap();
		openWrite();
		int rowDeleted = db.delete(CaseEntry.TABLE_NAME, CaseEntry._ID + "="
				+ id, null);
		if (rowDeleted != 0) {
			status = true;
		}
		close();
		return status;
	}
}
