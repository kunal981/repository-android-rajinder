package com.brst.alcohol.toxicology.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.brst.alcohol.toxicology.db.entry.CaseInfo.CaseEntry;
import com.brst.alcohol.toxicology.db.entry.DrinkInfo.DrinkTable;

public class DbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "alcoholtoxicology.db";
	private static final int DATABASE_VERSION = 1;

	private static final String TEXT_TYPE = " TEXT";
	private static final String REAL_TYPE = " REAL";
	private static final String COMMA_SEP = ",";
	// create table queries

	// Database creation sql statement
	private static final String SQL_CREATE_CUSTOM_DRINK = "CREATE TABLE "
			+ DrinkTable.TABLE_NAME + "(" + DrinkTable._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ DrinkTable.COLUMN_DRINK_TYPE + TEXT_TYPE + COMMA_SEP
			+ DrinkTable.COLUMN_DRINK_NAME + TEXT_TYPE + COMMA_SEP
			+ DrinkTable.COLUMN_ALCOHOL_PERCENTAGE + TEXT_TYPE + COMMA_SEP
			+ DrinkTable.COLUMN_ALCOHOL_VOLUME + TEXT_TYPE + ");";

	private static final String SQL_CREATE_CASE = "CREATE TABLE "
			+ CaseEntry.TABLE_NAME + "(" + CaseEntry._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + CaseEntry.NAME
			+ TEXT_TYPE + COMMA_SEP + CaseEntry.METHOD + TEXT_TYPE + COMMA_SEP
			+ CaseEntry.CREATED_ON + TEXT_TYPE + COMMA_SEP + CaseEntry.BAC_VAL
			+ TEXT_TYPE + ");";

	private static final String SQL_DELETE_DRINK_TABLE = "DROP TABLE IF EXISTS "
			+ DrinkTable.TABLE_NAME;
	private static final String SQL_DELETE_CASE_TABLE = "DROP TABLE IF EXISTS "
			+ CaseEntry.TABLE_NAME;

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_CUSTOM_DRINK);
		db.execSQL(SQL_CREATE_CASE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// This database is only a cache for online data, so its upgrade policy
		// is
		// to simply to discard the data and start over
		db.execSQL(SQL_DELETE_DRINK_TABLE);
		db.execSQL(SQL_DELETE_CASE_TABLE);
		onCreate(db);
	}

}
