package com.brst.alcohol.toxicology.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.brst.alcohol.toxicology.R;

public class BaseContainerFragment extends Fragment {

	Fragment fr;
	String fragmentPreviousback;
	String fragmentPrevious;
	String fragmentCurrent;

	public void replaceFragment(Fragment fragment, boolean addToBackStack) {
		fr = fragment;
		fragmentPreviousback = fragmentPrevious;
		fragmentPrevious = fragmentCurrent;
		fragmentCurrent = fr.getClass().getSimpleName();
		FragmentTransaction transaction = getChildFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.container_framelayout, fragment);
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}

	public boolean popFragment() {

		Log.i("Base", "pop fragment: "
				+ getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();
			fragmentCurrent = fragmentPrevious;

			for (Fragment fragment : getChildFragmentManager().getFragments()) {
				if (fragment.getClass().getSimpleName().equals(fragmentCurrent)) {
					fr = fragment;
					break;
				}

			}

			fragmentPrevious = fragmentPreviousback;
		}

		return isPop;
	}

	public boolean popFragmentAll() {

		boolean isPop = false;

		while (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();
		}

		return isPop;
	}

	public Fragment currentFragment() {
		return fr;
	}
}
