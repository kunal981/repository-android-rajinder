package com.brst.alcohol.toxicology.app;

import java.util.HashMap;
import java.util.List;

import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.modal.CaseRetrogradeProfileModel;
import com.brst.alcohol.toxicology.modal.Drink;

public class CaseDataAdapter {

	private static CaseDataAdapter cInstance;

	CaseProfileModal caseProfile;

	CaseRetrogradeProfileModel caseRetroGradeModel;

	HashMap<String, Drink> initDrinkData;

	/* Static 'instance' method */
	public static CaseDataAdapter getInstance() {
		return cInstance;
	}

	public static void initInstance() {
		if (cInstance == null) {
			cInstance = new CaseDataAdapter();
		}
	}

	public CaseProfileModal getCaseProfile() {
		return caseProfile;
	}

	public void setCaseProfile(CaseProfileModal caseProfile) {
		this.caseProfile = caseProfile;
	}

	public HashMap<String, Drink> getInitDrinkData() {
		return initDrinkData;
	}

	public void setInitDrinkData(HashMap<String, Drink> initDrinkData) {
		this.initDrinkData = initDrinkData;
	}

	public CaseRetrogradeProfileModel getCaseRetroGradeModel() {
		return caseRetroGradeModel;
	}

	public void setCaseRetroGradeModel(
			CaseRetrogradeProfileModel caseRetroGradeModel) {
		this.caseRetroGradeModel = caseRetroGradeModel;
	}

}
