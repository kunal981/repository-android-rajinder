package com.paddypower.android.app.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.paddypower.android.app.modal.Item;

public class FeedParser {

	private static final String ns = null;

	// We don't use namespaces

	public List<Item> parse(InputStream in) throws XmlPullParserException,
			IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readFeed(parser);
		} finally {
			in.close();
		}
	}

	private List<Item> readFeed(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		List<Item> entries = new ArrayList<Item>();

		parser.require(XmlPullParser.START_TAG, ns, "channel");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals("item")) {
				entries.add(readItem(parser));
			} else {
				skip(parser);
			}
		}
		return entries;
	}

	private Item readItem(XmlPullParser parser) throws XmlPullParserException,
			IOException {

		parser.require(XmlPullParser.START_TAG, ns, "item");
		String title = null;
		String link = null;
		String description = null;
		String selectionPrice = null;
		String eventCategory = null;
		String eventTitle = null;
		String selectionDescription = null;
		String eventId = null;
		String selectionId = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("title")) {
				title = readTitle(parser);
			} else if (name.equals("link")) {
				link = readLink(parser);
			} else if (name.equals("description")) {
				description = readDesc(parser);
			} else if (name.equals("selection:price")) {
				selectionPrice = readSelectionPrice(parser);
			} else if (name.equals("event:title")) {
				eventTitle = readEventTitle(parser);
			} else if (name.equals("event:category")) {
				eventCategory = readEventCategory(parser);
			} else if (name.equals("event:id")) {
				eventId = readEventId(parser);
			} else if (name.equals("selection:id")) {
				selectionId = readSelectionId(parser);
			} else if (name.equals("selection:description")) {
				selectionDescription = readSelectionDesc(parser);
			} else {
				skip(parser);
			}
		}
		return new Item(title, link, description, selectionPrice,
				eventCategory, eventTitle, selectionDescription, eventId,
				selectionId);

	}

	private String readDesc(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "description");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "description");
		return title;
	}

	private String readSelectionPrice(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "selection:price");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "selection:price");
		return title;
	}

	private String readEventTitle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "event:title");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "event:title");
		return title;
	}

	private String readEventCategory(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "event:category");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "event:category");
		return title;
	}

	private String readEventId(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "event:id");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "event:id");
		return title;
	}

	private String readSelectionId(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "selection:id");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "selection:id");
		return title;
	}

	private String readSelectionDesc(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "selection:description");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "selection:description");
		return title;
	}

	private String readTitle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "title");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "title");
		return title;
	}

	private String readLink(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "link");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "link");
		return title;
	}

	// For the tags title and summary, extracts their text values.
	private String readText(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

}
