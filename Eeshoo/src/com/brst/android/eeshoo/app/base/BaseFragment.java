package com.brst.android.eeshoo.app.base;

import java.util.HashMap;
import java.util.Stack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.brst.android.eeshoo.app.AppMainTabActivity;
import com.brst.android.eeshoo.app.R;

public class BaseFragment extends Fragment {
	public AppMainTabActivity mActivity;

	protected HashMap<String, Stack<Fragment>> mStacks;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = (AppMainTabActivity) this.getActivity();
		mStacks = mActivity.mStacks;
		if (mActivity.positionCurrentCategorySelected != -1) {
			mActivity.positionCurrentCategorySelected = -1;
		}
	}

	public void onBackPressed() {
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	}

	public void replaceFragment(Fragment fragment, boolean addToBackStack) {
		// fr = fragment;
		// fragmentPrevious = fragmentCurrent;
		// fragmentCurrent = fr.getClass().getSimpleName();
		FragmentTransaction transaction = getChildFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.container_framelayout, fragment);
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}

	public boolean popFragment() {

		Log.i("Base", "pop fragment: "
				+ getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();

		}

		return isPop;
	}

	public void pushFragments(String tag, Fragment fragment,
			boolean shouldAnimate, boolean shouldAdd) {
		if (shouldAdd)
			mStacks.get(tag).push(fragment);
		FragmentManager manager = getChildFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		if (shouldAnimate)
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		ft.replace(R.id.container_framelayout, fragment);
		if (shouldAnimate) {
			ft.addToBackStack(null);
		}
		ft.commit();
	}

	public void pushMenuFragments(String tag, Fragment fragment,
			boolean shouldAnimate, boolean shouldAdd) {
		if (shouldAdd)
			mStacks.get(tag).push(fragment);
		FragmentManager manager = getChildFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		ft.replace(R.id.container_framelayout, fragment);
		if (shouldAnimate) {
			ft.addToBackStack(null);
		}
		ft.commit();
	}

	public boolean popFragments(String mCurrentTab) {
		/*    
		 *    Select the second last fragment in current tab's stack.. 
		 *    which will be shown after the fragment transaction given below 
		 */
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();

		}
		if (mStacks.get(mCurrentTab).size() > 0) {
			mStacks.get(mCurrentTab).pop();
		}

		// if (mStacks.get(mCurrentTab).size() <= 1) {
		// isPop = false;
		// } else {
		// Fragment fragment = mStacks.get(mCurrentTab).elementAt(
		// mStacks.get(mCurrentTab).size() - 2);
		//
		// /*pop current fragment from stack.. */
		// mStacks.get(mCurrentTab).pop();
		//
		// /* We have the target fragment in hand.. Just show it.. Show a
		// standard navigation animation*/
		// FragmentManager manager = getChildFragmentManager();
		// FragmentTransaction ft = manager.beginTransaction();
		// ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
		// ft.replace(R.id.container_framelayout, fragment);
		// ft.commit();
		// isPop = true;
		// }

		return isPop;
	}

	public void popAllFragment(String mCurrentTab) {
		while (mStacks.get(mCurrentTab).size() > 1) {
			mStacks.get(mCurrentTab).pop();
		}
		getChildFragmentManager().popBackStack(null,
				FragmentManager.POP_BACK_STACK_INCLUSIVE);

	}

}