package com.brst.android.eeshoo.app.db.dao;

import java.sql.SQLException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.brst.android.eeshoo.app.db.DbConstant.User;
import com.brst.android.eeshoo.app.db.DbOperation;
import com.brst.android.eeshoo.app.model.UserDetail;

public class UserDAO extends DbOperation {

	private static final String WHERE_ID_EQUALS = User._ID + " =?";

	public UserDAO(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public long save(UserDetail userDetail) {
		long result = -1;
		if (!isUserExist(userDetail.getId())) {
			ContentValues values = new ContentValues();
			values.put(User._ID, userDetail.getId());
			values.put(User.USER_FRIST_NAME, userDetail.getFirstName());
			values.put(User.USER_LAST_NAME, userDetail.getFirstName());
			values.put(User.EMAIL, userDetail.getFirstName());
			values.put(User.PASSWORD, userDetail.getFirstName());
			result = database.insert(User.USER_TABLE, null, values);
		} else {
			result = update(userDetail);
		}
		return result;
	}

	public long update(UserDetail userDetail) {
		long result = -1;
		ContentValues values = new ContentValues();
		values.put(User.USER_FRIST_NAME, userDetail.getFirstName());
		values.put(User.USER_LAST_NAME, userDetail.getFirstName());
		values.put(User.EMAIL, userDetail.getFirstName());
		values.put(User.PASSWORD, userDetail.getFirstName());

		result = database.update(User.USER_TABLE, values, WHERE_ID_EQUALS,
				new String[] { String.valueOf(userDetail.getId()) });
		Log.d("Update Row Effected :", "=" + result);
		return result;

	}

	public int deleteUser(UserDetail userDetail) {
		return database.delete(User.USER_TABLE, WHERE_ID_EQUALS,
				new String[] { userDetail.getId() + "" });
	}

	public boolean isUserExist(long id) {

		boolean status = false;
		String[] projection = { User._ID, User.EMAIL };

		Cursor cursor = database.query(User.USER_TABLE, // The table to query
				projection, // The columns to return
				WHERE_ID_EQUALS, // The columns for the WHERE clause
				new String[] { String.valueOf(id) }, // The values for the WHERE
														// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		if (cursor != null && cursor.moveToFirst()) {
			status = true;
		}
		return status;

	}

	public UserDetail getUser(String email) throws SQLException {
		// hp = new HashMap();
		UserDetail ud = new UserDetail();
		String[] projection = { User._ID, User.USER_FRIST_NAME,
				User.USER_FRIST_NAME, User.EMAIL, User.PASSWORD };

		String sortOrder = User._ID + " ASC";

		Cursor cursor = database.query(User.USER_TABLE, // The table to query
				projection, // The columns to return
				User.EMAIL, // The columns for the WHERE clause
				new String[] { email }, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			ud.setId(cursor.getLong(cursor.getColumnIndexOrThrow(User._ID)));
			ud.setFirstName(cursor.getString(cursor
					.getColumnIndexOrThrow(User.USER_FRIST_NAME)));
			ud.setLastName(cursor.getString(cursor
					.getColumnIndexOrThrow(User.USER_LAST_NAME)));
			ud.setEmail(cursor.getString(cursor
					.getColumnIndexOrThrow(User.EMAIL)));
			ud.setPassword(cursor.getString(cursor
					.getColumnIndexOrThrow(User.PASSWORD)));
			cursor.moveToNext();
		}
		return ud;
	}
}
