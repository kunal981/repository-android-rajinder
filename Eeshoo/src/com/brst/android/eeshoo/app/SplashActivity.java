package com.brst.android.eeshoo.app;

import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.eeshoo.app.controller.AppController;
import com.brst.android.eeshoo.app.home.HomeActivity;
import com.brst.android.eeshoo.app.util.AppConstants;
import com.brst.android.eeshoo.app.util.ConnectionDetector;
import com.brst.android.eeshoo.app.util.UiWidget;
import com.brst.android.eeshoo.app.util.WSConstant;

public class SplashActivity extends Activity {

	private int SPLASH_TIME_OUT = 1000;
	SharedPreferences sharedpreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		sharedpreferences = getSharedPreferences(AppConstants.EESHOO,
				Context.MODE_PRIVATE);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				Intent intent_home;

				if (sharedpreferences != null
						&& sharedpreferences.contains("email")
						&& sharedpreferences.contains("password")
						&& sharedpreferences.contains("customer_id")) {
					intent_home = new Intent(SplashActivity.this,
							AppMainTabActivity.class);

				} else {
					intent_home = new Intent(SplashActivity.this,
							LoginActivity.class);
				}
				// intent_home = new Intent(SplashActivity.this,
				// LoginActivity.class);
				startActivity(intent_home);
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
				finish();
			}
		}, SPLASH_TIME_OUT);

	}

}
