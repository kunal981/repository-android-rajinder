package com.brst.android.eeshoo.app.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.brst.android.eeshoo.app.LoginActivity.AddUserTask;
import com.brst.android.eeshoo.app.model.BookCategory;
import com.brst.android.eeshoo.app.model.BookDetail;
import com.brst.android.eeshoo.app.model.UserDetail;

public class JsonParser {

	private static final String TAG = JsonParser.class.getName();

	public static List<BookCategory> getAllCategory(String response) {
		List<BookCategory> listBookCategory = new ArrayList<>();

		try {
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray jsonArraryProduct = jsonOBject
						.getJSONArray("categories");

				for (int i = 0; i < jsonArraryProduct.length(); i++) {
					JSONObject jsonProductObj = jsonArraryProduct
							.getJSONObject(i);
					BookCategory bookCategoruy = new BookCategory();
					bookCategoruy.setId(Long.parseLong(jsonProductObj
							.getString("id")));
					bookCategoruy.setName(jsonProductObj.getString("name"));
					bookCategoruy.setImage(jsonProductObj.getString("image"));
					listBookCategory.add(bookCategoruy);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listBookCategory;

	}

	public static List<BookDetail> getUserPurchasedBookList(String response) {
		List<BookDetail> listBook = new ArrayList<>();

		try {
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				String userId = null;

				if (jsonOBject.has("customer_id")) {
					userId = jsonOBject.getString("customer_id");
				}

				if (jsonOBject.has("orders")) {
					JSONArray jBookArrary = jsonOBject.getJSONArray("orders");
					for (int i = 0; i < jBookArrary.length(); i++) {
						JSONObject jObjBook = jBookArrary.getJSONObject(i);
						BookDetail detail = new BookDetail();
						detail.setProductId(jObjBook.getString("product_id"));
						detail.setName(jObjBook.getString("name"));
						detail.setAuthor(jObjBook.getString("author"));
						detail.setDescription(jObjBook.getString("description"));
						detail.setImage(jObjBook.getString("image"));
						detail.setUserId(userId);
						listBook.add(detail);
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listBook;

	}

	public static List<BookDetail> getCategoryProduct(String response) {
		List<BookDetail> listBook = new ArrayList<>();

		try {
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				if (jsonOBject.has("products")) {
					JSONArray jBookArrary = jsonOBject.getJSONArray("products");
					for (int i = 0; i < jBookArrary.length(); i++) {
						JSONObject jObjBook = jBookArrary.getJSONObject(i);
						BookDetail detail = new BookDetail();
						detail.setProductId(jObjBook.getString("id"));
						detail.setName(jObjBook.getString("name"));
						detail.setAuthor(jObjBook.getString("author"));
						if (jObjBook.has("des")) {
							detail.setDescription(jObjBook.getString("des"));
						} else if (jObjBook.has("des")) {
							detail.setDescription(jObjBook
									.getString("description"));

						}
						detail.setImage(jObjBook.getString("image"));
						listBook.add(detail);
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listBook;

	}

	public static BookDetail getBookDetail(String response) {

		BookDetail bookDetail = new BookDetail();

		try {
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				JSONObject jsonProduct = jsonOBject.getJSONObject("product");
				bookDetail.setName(jsonProduct.getString("name"));
				bookDetail.setPrice(jsonProduct.getString("price"));
				bookDetail.setDescription(jsonProduct.getString("description"));
				bookDetail.setImage(jsonProduct.getString("image"));
				bookDetail.setAuthor(jsonProduct.getString("author"));
				bookDetail
						.setPublishDate(jsonProduct.getString("publish_date"));
				bookDetail.setPath(jsonProduct.getString("path"));
				bookDetail.setIsbn(jsonProduct.getString("isbn"));

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bookDetail;

	}
}
