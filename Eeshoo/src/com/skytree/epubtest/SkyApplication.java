package com.skytree.epubtest;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.brst.android.eeshoo.app.model.BookDetail;
import com.skytree.epub.BookInformation;

public class SkyApplication {
	public String message = "We are the world.";
	public ArrayList<BookInformation> bis;
	public ArrayList<CustomFont> customFonts = new ArrayList<CustomFont>();
	public SkySetting setting;
	public SkyDatabase sd = null;
	public int sortType = 0;
	public static SkyApplication uInstance;

	public SkyApplication(Context context) {
		sd = new SkyDatabase(context);
		// reloadBookInformations();
		loadSetting();
	}

	List<BookDetail> listUserPurchasedBook;

	/* Static 'instance' method */
	public static SkyApplication getInstance() {
		return uInstance;
	}

	public static void initInstance(Context context) {
		if (uInstance == null) {
			uInstance = new SkyApplication(context);
		}
	}

	public void reloadBookInformations() {
		this.bis = sd.fetchBookInformations(sortType, "");
	}

	public void reloadBookInformations(String key) {
		this.bis = sd.fetchBookInformations(sortType, key);
	}

	public void loadSetting() {
		this.setting = sd.fetchSetting();
	}

	public void saveSetting() {
		sd.updateSetting(this.setting);
	}
}
