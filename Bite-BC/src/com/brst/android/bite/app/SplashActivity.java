package com.brst.android.bite.app;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.constant.WSConstant;
import com.brst.android.bite.app.constant.AppConstant.BiteBc;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.UserDataHandler;
import com.brst.android.bite.app.domain.User;
import com.brst.android.bite.app.home.HomeActivity;
import com.brst.android.bite.app.membership.MembershipActivity;
import com.brst.android.bite.app.util.ConnectionDetector;
import com.brst.android.bite.app.util.UI;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;

public class SplashActivity extends Activity {

	private UiLifecycleHelper uiHelper;
	private static final String TAG = "MainFragment";
	private static final int TAG_1 = 300;
	private static final int TAG_2 = 301;

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;

	private int SPLASH_TIME_OUT = 700;

	SharedPreferences sharedpreferences;
	private UserDataHandler uDataHandler;
	private User user;
	String userId;
	String pwd;
	boolean isStatus = false;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		uDataHandler = UserDataHandler.getInstance();

		cd = new ConnectionDetector(getApplicationContext());
		isInternetPresent = cd.isConnectingToInternet();
		sharedpreferences = getSharedPreferences(BiteBc.MyPREFERENCES,
				Context.MODE_PRIVATE);

		if (sharedpreferences != null
				&& sharedpreferences.contains(BiteBc.USER_ID)
				&& sharedpreferences.contains(BiteBc.USER_PWD)) {
			uiHelper = new UiLifecycleHelper(this, callback);
			uiHelper.onCreate(savedInstanceState);

		} else {
			uiHelper = new UiLifecycleHelper(this, callback);
			uiHelper.onCreate(savedInstanceState);
			openMainActivity(TAG_1);
		}

		// new Handler().postDelayed(new Runnable() {
		// /*
		// * Showing splash screen with a timer. This will be useful when you
		// * want to show case your app logo / company
		// */
		// @Override
		// public void run() {
		// checkInternet(isInternetPresent);
		// // This method will be executed once the timer is over
		// // Start your app main activity
		//
		// // Intent intent_home = new Intent(SplashActivity.this,
		// // MainActivity_2.class);
		// //
		// // startActivity(intent_home);
		// // overridePendingTransition(R.anim.slide_in_left,
		// // R.anim.slide_out_right);
		// // finish();
		// }
		// }, SPLASH_TIME_OUT);

	}

	private void openMainActivity(final int tag) {

		new Handler().postDelayed(new Runnable() {
			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */
			@Override
			public void run() {
				checkInternet(isInternetPresent, tag);
				// This method will be executed once the timer is over
				// Start your app main activity

				// Intent intent_home = new Intent(SplashActivity.this,
				// MainActivity_2.class);
				//
				// startActivity(intent_home);
				// overridePendingTransition(R.anim.slide_in_left,
				// R.anim.slide_out_right);
				// finish();
			}
		}, SPLASH_TIME_OUT);

	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {

		if (!isStatus) {
			isStatus = true;
			if (session != null && session.isOpened()) {
				 loginWithoutFb(TAG_2);
//				openMainActivity(TAG_1);
			} else {
				openMainActivity(TAG_1);
			}

		}
	}

	private void loginWithoutFb(int tag2) {
		checkInternet(isInternetPresent, tag2);
	}

	private void loginFromPreferences() {

		userId = sharedpreferences.getString(BiteBc.USER_ID, "");
		pwd = sharedpreferences.getString(BiteBc.USER_PWD, "");
		HashMap<String, String> params = new HashMap<>();
		params.put(DataKey.USER_ID, userId);
		params.put(DataKey.USER_PWD, pwd);
		makeRegisterRequest(WSConstant.Web.LOGIN, params);

	}

	private void checkInternet(Boolean isInternetPresent2, int tag) {
		// check for Internet status
		if (isInternetPresent2) {

			Intent intent_home = null;
			// Internet Connection is Present
			// make HTTP requests
			switch (tag) {
			case TAG_1:
				intent_home = new Intent(SplashActivity.this,
						MainActivity_2.class);
				startActivity(intent_home);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
				finish();
				break;
			case TAG_2:
				loginFromPreferences();
				break;

			default:
				break;
			}

		} else {
			// Internet connection is not present
			// Ask user to connect to Internet
			showAlertDialog(SplashActivity.this, "No Internet Connection",
					"You don't have internet connection.", false, tag);
		}

	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message,
			Boolean status, final int tag) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
						isInternetPresent = cd.isConnectingToInternet();
						checkInternet(isInternetPresent, tag);

					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
								SplashActivity.this.finish();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeRegisterRequest(String login,
			final Map<String, String> rParams) {

		String url = Web.HOST + login;
		UI.showProgressDialog(this);
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						// Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());
						UI.hideProgressDialog();
						login(response);

					}

				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			/**
			 * Passing some request headers
			 * */
			// @Override
			// public Map<String, String> getHeaders() throws AuthFailureError {
			// HashMap<String, String> headers = new HashMap<String, String>();
			// headers.put("Content-Type", "application/json");
			// return headers;
			// }

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.DataKey.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	protected void login(String response) {
		try {
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				user = new User();
				String customerId = jsonOBject.getString("customerid");
				String session = jsonOBject.getString("sessionid");
				String firstName = jsonOBject.getString("firstname");
				String lastName = jsonOBject.getString("lastname");

				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setCustomerid(customerId);
				user.setUserSessionId(session);
				user.setEmail(userId);
				user.setuFacebookId(pwd);
				uDataHandler.setUser(user);
				showMembershipActivity();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showMembershipActivity() {

		Intent intent;
		if (sharedpreferences != null
				&& sharedpreferences.contains(BiteBc.USER_PLAN)) {
			intent = new Intent(this, HomeActivity.class);
		} else {
			intent = new Intent(this, MembershipActivity.class);
		}
		// intent = new Intent(getActivity(), MembershipActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();

	}

	/**
	 * Facebook lifecycle method must handles
	 */

	@Override
	public void onResume() {
		super.onResume();

		uiHelper.onResume();
		if (!isStatus) {
			Session session = Session.getActiveSession();
			if (session != null && (session.isOpened() || session.isClosed())) {
				onSessionStateChange(session, session.getState(), null);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	// Facebook lifecycle ends

}
