package com.brst.android.bite.app.locate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.base.BaseContainerFragment;
import com.brst.android.bite.app.constant.WSConstant;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.domain.Restaurant;
import com.brst.android.bite.app.home.RestaurantDetailMain;
import com.brst.android.bite.app.search.SearchFragment;
import com.brst.android.bite.app.util.GPSTracker;
import com.brst.android.bite.app.util.JsonParser;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class NearMeFragment extends Fragment implements
		OnInfoWindowClickListener {

	private static final String MAP_FRAGMENT_TAG = "map";

	private GoogleMap mMap;
	SupportMapFragment mSupportMapFragment;
	TextView textSearch;
	private static String TAG = "Map";

	ProgressDialog pDialog;

	LocationManager locationManager;

	boolean isGPSEnabled, isNetworkEnabled;
	boolean canGetLocation;

	long MIN_TIME_BW_UPDATES = 1000;
	float MIN_DISTANCE_CHANGE_FOR_UPDATES = 0f;

	List<Restaurant> listRestaurants;

	List<LatLng> listLat = new ArrayList<LatLng>();

	private Marker marker;
	HashMap<String, Restaurant> markers;
	HashMap<String, String> markersUrl;
	RestaurantDataHandler rDataHandler;

	ImageLoader imageLoader;

	HashMap<String, Bitmap> mapBimap;

	// GPSTracker class
	GPSTracker gps;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		gps = new GPSTracker(getActivity());

		imageLoader = AppController.getInstance().getImageLoader();
		listRestaurants = new ArrayList<Restaurant>();
		mapBimap = new HashMap<>();
		listLat = new ArrayList<LatLng>();
		markers = new HashMap<String, Restaurant>();
		markersUrl = new HashMap<String, String>();
		rDataHandler = RestaurantDataHandler.getInstance();

		intLatLong();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragement_layout_restaurant_near_me, null);

		if (!gps.canGetLocation()) {
			gps.showSettingsAlert();
		}

		textSearch = (EditText) rootView.findViewById(R.id.search_id);

		addtextSearchListner();

		initMap();

		Log.e(TAG, "Lat::" + String.valueOf(gps.getLatitude()));
		Log.e(TAG, "Long::" + String.valueOf(gps.getLongitude()));
		makeRequestForRestaurantByLatLong(WSConstant.Web.NEAR_ME,
				String.valueOf(gps.getLatitude()),
				String.valueOf(gps.getLongitude()));

		return rootView;
	}

	private void initMap() {
		int status = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());

		// Showing status
		if (status != ConnectionResult.SUCCESS) { // Google Play Services are
			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
					getActivity(), requestCode);
			dialog.show();

		} else {
			if (mMap == null) {
				mSupportMapFragment = (SupportMapFragment) getFragmentManager()
						.findFragmentByTag(MAP_FRAGMENT_TAG);

				if (mSupportMapFragment == null) {
					// To programmatically add the map, we first create a
					// SupportMapFragment.
					mSupportMapFragment = SupportMapFragment.newInstance();

					// Then we add it using a FragmentTransaction.
					FragmentTransaction fragmentTransaction = getFragmentManager()
							.beginTransaction();
					fragmentTransaction.replace(R.id.id_map_container,
							mSupportMapFragment, MAP_FRAGMENT_TAG);
					fragmentTransaction.commit();
				}
			}
			setUpMapIfNeeded();

		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mMap != null) {
			mMap = null;
		}
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = mSupportMapFragment.getMap();

			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		// LatLng latLng = new LatLng(43.67023, -79.38676);
		LatLng latLng = getCurrentPosition();

		// Showing the current location in Google Map
		mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		mMap.animateCamera(CameraUpdateFactory.zoomTo(5));
		mMap.setMyLocationEnabled(true);
		// mMap.addMarker(new MarkerOptions().position(latLng)
		// .title("Vancover Nation").snippet("Vancover City"));

	}

	private LatLng getCurrentPosition() {

		double latitude = gps.getLatitude();
		double longitude = gps.getLongitude();
		LatLng latLng = new LatLng(latitude, longitude);

		return latLng;
	}

	@Override
	public void onResume() {
		super.onResume();
		// In case Google Play services has since become available.
		setUpMapIfNeeded();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	private void intLatLong() {

		LatLng van = new LatLng(49.249660, -123.1193400);
		LatLng t2 = new LatLng(45.24, -75.41);
		LatLng t3 = new LatLng(49.8994, -97.1392);
		LatLng t4 = new LatLng(49.249660, -123.1193400);
		LatLng t5 = new LatLng(43.67023, -79.38676);
		LatLng t6 = new LatLng(50.27, -104.36);
		LatLng t7 = new LatLng(52.56, -73.32676);

		listLat.add(t4);
		listLat.add(t2);
		listLat.add(t3);
		listLat.add(t5);
		listLat.add(van);
		listLat.add(t6);
		listLat.add(t7);

	}

	private void addtextSearchListner() {

		textSearch
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							if (!textSearch.getText().toString().trim()
									.equals("")) {
								makeRequestForRestaurant(Web.NEAR_BY,
										textSearch.getText().toString().trim());
								UI.hideKeyboard(getActivity(), textSearch);
							} else {
								LogMsg.LOG(getActivity(), "Empty fields");
							}
							return true;
						}
						return false;
					}
				});

		textSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				textSearch.setCompoundDrawables(null, null, null, null);
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

	}

	// Service to fetch restaurant list on the basis of pin

	private void makeRequestForRestaurant(String product,
			final String searchString) {

		String url = Web.HOST + product;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						parseResturant(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();

				params.put(DataKey.DATA_SEARCH, searchString);

				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	protected void parseResturant(String response) {
		// TODO Auto-generated method stub
		UI.hideProgressDialog();

		listRestaurants = JsonParser.getRestaurantsDetail(response);

		if (listRestaurants != null && listRestaurants.size() != 0) {
			upDateMapElement();
		} else {
			Toast.makeText(getActivity(), "No data to Update",
					Toast.LENGTH_LONG).show();
		}

	}

	private void upDateMapElement() {
		LatLng latLng = null;

		for (int i = 0; i < listRestaurants.size(); i++) {
			Restaurant r = listRestaurants.get(i);
			latLng = new LatLng(Double.parseDouble(r.getLatitude()),
					Double.parseDouble(r.getLongitude()));
			marker = mMap.addMarker(new MarkerOptions().position(latLng)
					.title(r.getName()).snippet(r.getAddress()));
			markers.put(marker.getId(), r);
			markersUrl.put(marker.getId(), r.getImage());
		}

		mMap.setInfoWindowAdapter(new InfoWindowAdapterMarker(getActivity()));
		mMap.setOnInfoWindowClickListener(this);
		mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		mMap.animateCamera(CameraUpdateFactory.zoomTo(6));

	}

	private void makeRequestForRestaurantByLatLong(String product,
			final String lat, final String lang) {

		String url = Web.HOST + product;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						parseResturant(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put(DataKey.LAT, lat);
				params.put(DataKey.LONG, lang);
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	public class InfoWindowAdapterMarker implements InfoWindowAdapter {

		private Context mContext;

		public InfoWindowAdapterMarker(Context context) {
			mContext = context;
		}

		@Override
		public View getInfoContents(final Marker marker) {

			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.custom_info_window, null);

			final ImageView image = ((ImageView) view.findViewById(R.id.badge));
			final TextView titleUi = ((TextView) view.findViewById(R.id.title));
			final TextView snippetUi = ((TextView) view
					.findViewById(R.id.snippet));

			if (marker != null && marker.getId() != null) {

				titleUi.setText(marker.getTitle());
				snippetUi.setText(marker.getSnippet());
				// image.setImageBitmap(mapBimap.get(marker.getId()));

				imageLoader.get(markersUrl.get(marker.getId()),
						new ImageListener() {

							@Override
							public void onErrorResponse(VolleyError error) {
								Log.e(TAG,
										"Image Load Error: "
												+ error.getMessage());
							}

							@Override
							public void onResponse(ImageContainer response,
									boolean arg1) {
								if (response.getBitmap() != null) {
									image.setImageBitmap(response.getBitmap());
									// load image into imageview
									// mapBimap.put(markerId,
									// response.getBitmap());
									// marker.showInfoWindow();
								}
							}
						});

			}

			return view;
		}

		@Override
		public View getInfoWindow(Marker arg0) {

			return null;
		}

	}

	@Override
	public void onInfoWindowClick(Marker arg0) {
		Log.e(TAG, markers.get(marker.getId()).getId());
		Restaurant res = markers.get(marker.getId());
		if (res != null) {
			rDataHandler.setRestaurant(res);
			// RestaurantDetailMain fragment = new RestaurantDetailMain();
			// ((BaseContainerFragment) getParentFragment()).replaceFragment(
			// fragment, true);
			RestaurantDetailMain fragment = new RestaurantDetailMain();
			// getActivity().getFragmentManager().beginTransaction()
			// .replace(R.id.container_framelayout, fragment);
			((BaseContainerFragment) getParentFragment()).replaceFragmentMap(
					fragment, true);
		} else {
			LogMsg.LOG(getActivity(), "No Info Available");
		}
	}

}
