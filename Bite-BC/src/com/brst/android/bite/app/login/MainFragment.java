package com.brst.android.bite.app.login;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.MainActivity;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.constant.WSConstant;
import com.brst.android.bite.app.constant.AppConstant.BiteBc;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.UserDataHandler;
import com.brst.android.bite.app.domain.User;
import com.brst.android.bite.app.home.HomeActivity;
import com.brst.android.bite.app.membership.MembershipActivity;
import com.brst.android.bite.app.util.UI;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class MainFragment extends Fragment {

	private static final String TAG = "MainFragment";
	SharedPreferences sharedpreferences;

	private UiLifecycleHelper uiHelper;
	private UserDataHandler uDataHandler;
	private User user;

	private boolean isResumed = false;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uDataHandler = UserDataHandler.getInstance();
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
		sharedpreferences = getActivity().getSharedPreferences(
				BiteBc.MyPREFERENCES, Context.MODE_PRIVATE);

	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, container, false);

		LoginButton authButton = (LoginButton) view
				.findViewById(R.id.login_button);
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("user_likes", "email"));
		return view;
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (isResumed) {
			if (state.isOpened()) {
				Log.i(TAG, "Logged in...");
				makeMeRequest(session);
			} else if (state.isClosed()) {
				Log.i(TAG, "Logged out...");
			}
		}
	}

	/**
	 * Make a request for user Email id and information
	 * 
	 * @param session
	 */
	private void makeMeRequest(final Session session) {
		// Make an API call to get user data and define a
		// new callback to handle the response.
		UI.showProgressDialog(getActivity());
		Request request = Request.newMeRequest(session,
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						// If the response is successful
						if (session == Session.getActiveSession()) {
							if (user != null) {
								// Set the id for the ProfilePictureView
								// view that in turn displays the profile
								// picture.

								Log.e(TAG, user.getInnerJSONObject().toString());
								JSONObject jObject = user.getInnerJSONObject();
								registerOrLoginUser(jObject);

							}
						}
						if (response.getError() != null) {
							UI.hideProgressDialog();
							// Handle errors, will do so later.
						}
					}
				});
		request.executeAsync();
	}

	/**
	 * Register user to bite bc
	 * 
	 * @param jObject
	 */
	protected void registerOrLoginUser(JSONObject jObject) {

		// //Log.e.e(TAG, "");
		try {
			user = uDataHandler.getUser();

			if (user == null) {
				user = new User();
			}
			String firstname = jObject.getString("first_name");
			String lastname = jObject.getString("last_name");
			String email = jObject.getString("email");
			String pwd = jObject.getString("id");

			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setEmail(email);
			user.setuFacebookId(pwd);
			Map<String, String> params = new HashMap<String, String>();
			params.put("firstname", firstname);
			params.put("lastname", lastname);
			params.put("email", email);
			params.put("password", pwd);
			makeRegisterRequest(Web.REGISTER, params);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	protected void login(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {

				String customerId = jsonOBject.getString("customerid");
				String session = jsonOBject.getString("sessionid");
				user.setCustomerid(customerId);
				user.setUserSessionId(session);
				Editor editor = sharedpreferences.edit();
				editor.putString(BiteBc.USER_ID, user.getEmail());
				editor.putString(BiteBc.USER_PWD, user.getuFacebookId());
				editor.commit();
				uDataHandler.setUser(user);
				showMembershipActivity();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void showMembershipActivity() {

		Intent intent;
		// if (sharedpreferences != null
		// && sharedpreferences.contains(BiteBc.USER_PLAN)) {
		// intent = new Intent(getActivity(), HomeActivity.class);
		// } else {
		// intent = new Intent(getActivity(), MembershipActivity.class);
		// }
		intent = new Intent(getActivity(), MembershipActivity.class);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.slide_in_left,
				R.anim.slide_out_right);
		getActivity().finish();

	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeRegisterRequest(String register,
			final Map<String, String> rParams) {

		String url = Web.HOST + register;

		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						// Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());
						UI.hideProgressDialog();
						login(response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			/**
			 * Passing some request headers
			 * */
			// @Override
			// public Map<String, String> getHeaders() throws AuthFailureError {
			// HashMap<String, String> headers = new HashMap<String, String>();
			// headers.put("Content-Type", "application/json");
			// return headers;
			// }

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.DataKey.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	/**
	 * Facebook lifecycle method must handles
	 */

	@Override
	public void onResume() {
		super.onResume();
		// Session session = Session.getActiveSession();
		// if (session != null && (session.isOpened() || session.isClosed())) {
		// onSessionStateChange(session, session.getState(), null);
		// }
		uiHelper.onResume();
		isResumed = true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	// Facebook lifecycle ends

}
