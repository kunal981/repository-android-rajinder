package com.brst.android.bite.app.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.base.BaseContainerFragment;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.domain.Restaurant;
import com.brst.android.bite.app.home.RestaurantDetailMain;
import com.brst.android.bite.app.util.JsonParser;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;

public class SearchFragment extends Fragment implements OnItemClickListener {
	private List<Restaurant> lisItems;

	private final String TAG = "SearchFragment";

	EditText textSearch;
	TextView noData;

	ListView listView;

	RestuarantListAdapter adapter;

	ProgressDialog pDialog;

	RestaurantDataHandler rHandler;
	ImageLoader imageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = AppController.getInstance().getImageLoader();
		lisItems = new ArrayList<Restaurant>();
		rHandler = RestaurantDataHandler.getInstance();
		adapter = new RestuarantListAdapter(getActivity(), lisItems);
		// makeRequestForRestaurant(Web.PRODUCT); // now working no need to
		// fetch all restaurant

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_layout_search, null);
		((TextView) rootView.findViewById(R.id.header_title))
				.setText(R.string.search_header);
		textSearch = (EditText) rootView.findViewById(R.id.search_id);
		noData = (TextView) rootView.findViewById(R.id.noData);
		addtextSearchListner();

		listView = (ListView) rootView.findViewById(R.id.list_exp_view);
		listView.setOnItemClickListener(this);
		adapter = new RestuarantListAdapter(getActivity(), lisItems);
		listView.setAdapter(adapter);

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/*
	 * class methods including resource initialization and all start here
	 */

	private void addtextSearchListner() {

		textSearch
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							if (!textSearch.getText().toString().trim()
									.equals("")) {
								makeRequestForRestaurant(Web.SEARCH, textSearch
										.getText().toString().trim());
								if (UI.hideKeyboard(getActivity(), textSearch)) {
									// textSearch.setText("");
								}
							} else {
								LogMsg.LOG(getActivity(), "Empty fields");
							}
							return true;
						}
						return false;
					}
				});

		textSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				textSearch.setCompoundDrawables(null, null, null, null);
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

	}

	// response handler fro service
	protected void parseResturant(String response) {

		UI.hideProgressDialog();

		lisItems = JsonParser.getRestaurantsDetail(response);

		sortList(lisItems);

		if (lisItems != null && lisItems.size() != 0) {
			listView.setVisibility(View.VISIBLE);
			noData.setVisibility(View.GONE);
			adapter.addItems(lisItems);
		} else {
			lisItems = new ArrayList<Restaurant>();
			listView.setVisibility(View.GONE);
			noData.setVisibility(View.VISIBLE);
			LogMsg.LOG(getActivity(), "No search Availalbe");
		}

	}

	// sorting list
	private void sortList(List<Restaurant> listItem) {

		Collections.sort(listItem, new Comparator<Restaurant>() {
			@Override
			public int compare(Restaurant r1, Restaurant r2) {
				return r1.getName().compareTo(r2.getName());
			}
		});

	}

	// all dataadabpater for listview and other layout

	class RestuarantListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		List<Restaurant> listItems;

		public RestuarantListAdapter(Activity activity,
				List<Restaurant> listsItems) {
			this.activity = activity;
			this.listItems = listsItems;
		}

		public void addItems(List<Restaurant> listItems) {
			this.listItems = listItems;
			notifyDataSetChanged();

		}

		@Override
		public int getCount() {

			return listItems.size();
		}

		@Override
		public Object getItem(int position) {

			return listItems.get(position);
		}

		@Override
		public long getItemId(int id) {

			return id;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			final ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.layout_list_restaurant_item, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.text_r_name);
				holder.cusine = (TextView) convertView
						.findViewById(R.id.text_r_c_type);

				holder.image = (ImageView) convertView
						.findViewById(R.id.image_id);
				// holder.iconAdd.setImageResource(R.drawable.btn_plus);
				holder.offer = (TextView) convertView
						.findViewById(R.id.discount);
				holder.rate = (RatingBar) convertView
						.findViewById(R.id.rating_rest);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final Restaurant item = listItems.get(position);
			holder.name.setText(item.getName());
			holder.cusine.setText(item.getCuisine());
			holder.offer.setText(item.getOfferAvailable());
			holder.rate.setProgress(item.getRating());
			holder.name.setText(item.getName());

			// If you are using normal ImageView
			imageLoader.get(item.getImage(), new ImageListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e(TAG, "Image Load Error: " + error.getMessage());
				}

				@Override
				public void onResponse(ImageContainer response, boolean arg1) {
					if (response.getBitmap() != null) {
						// load image into imageview
						holder.image.setImageBitmap(response.getBitmap());
					}
				}
			});

			return convertView;
		}
	}

	static class ViewHolder {
		TextView name;
		TextView cusine;
		ImageView image;
		TextView offer;
		RatingBar rate;
	}

	/**
	 * All services call using volley * @param product
	 * 
	 * @param searchString
	 */
	private void makeRequestForRestaurant(String product,
			final String searchString) {

		String url = Web.HOST + product;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						parseResturant(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();

				params.put(DataKey.DATA_SEARCH, searchString);

				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	/**
	 * Not working no need to fetch all restaurant on activity start
	 * 
	 * @param response
	 */

	// private void makeRequestForRestaurant(String product) {
	//
	// String url = Web.HOST + product;
	// showProgressDialog();
	// StringRequest strReq = new StringRequest(Method.GET, url,
	// new com.android.volley.Response.Listener<String>() {
	//
	// @Override
	// public void onResponse(String response) {
	// Log.e(TAG + "On Response", response);
	// parseResturant(response);
	// }
	// }, new com.android.volley.Response.ErrorListener() {
	//
	// @Override
	// public void onErrorResponse(VolleyError error) {
	// VolleyLog.e(TAG, "Error: " + error.getMessage());
	// hideProgressDialog();
	// }
	// });
	//
	// // Adding request to request queue
	// AppController.getInstance().addToRequestQueue(strReq,
	// DataKey.TAG_JSON_OBJECT);
	//
	// }

	// all click listener starts here

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Restaurant item = lisItems.get(position);
		rHandler.setRestaurant(item);

		RestaurantDetailMain fragment = new RestaurantDetailMain();
		((BaseContainerFragment) getParentFragment()).replaceFragment(fragment,
				true);

	}

}
