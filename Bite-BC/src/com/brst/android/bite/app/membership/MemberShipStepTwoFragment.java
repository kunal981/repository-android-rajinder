package com.brst.android.bite.app.membership;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.constant.WSConstant;
import com.brst.android.bite.app.constant.AppConstant.BiteBc;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.UserDataHandler;
import com.brst.android.bite.app.domain.User;
import com.brst.android.bite.app.home.HomeActivity;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

public class MemberShipStepTwoFragment extends Fragment implements
		OnClickListener {

	public static String PUBLISHABLE_KEY = null;

	private static String TAG = MemberShipStepTwoFragment.class.getName();

	EditText cardNumber, cvcNUmber;

	Spinner cardType, sExpYear, sEMonthYear;
	SpinnerAdapter adapterCType, adapterExpYear, adapterMonth,
			adapterStartYear;

	Button submit;

	UserDataHandler uDataHandler;

	User user;

	SharedPreferences sharedpreferences;

	String cardNumberValue, cardCVCNumber;
	Integer cardExpiryYear, cardExpiryMonth;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uDataHandler = UserDataHandler.getInstance();
		user = uDataHandler.getUser();
		PUBLISHABLE_KEY = getPublishKeyFromStripe();
		sharedpreferences = getActivity().getSharedPreferences(
				BiteBc.MyPREFERENCES, Context.MODE_PRIVATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_step_two,
				container, false);

		cardNumber = (EditText) rootView.findViewById(R.id.card_number);
		cvcNUmber = (EditText) rootView.findViewById(R.id.cvc_number);

		cardType = (Spinner) rootView.findViewById(R.id.card_type);
		sExpYear = (Spinner) rootView.findViewById(R.id.expire_year);
		sEMonthYear = (Spinner) rootView.findViewById(R.id.expire_month);

		addTextChangeLisner();
		addSpinnerItemListner();

		submit = (Button) rootView.findViewById(R.id.btn_submit);
		submit.setOnClickListener(this);

		String[] cardTypeItem = getResources().getStringArray(
				R.array.array_card_type);

		String[] expYearItem = getResources().getStringArray(
				R.array.arrary_year_e);
		String[] monthItem = getResources()
				.getStringArray(R.array.arrary_month);

		adapterCType = new SpinnerAdapter(getActivity(),
				R.layout.layout_spinner_item, cardTypeItem);
		adapterExpYear = new SpinnerAdapter(getActivity(),
				R.layout.layout_spinner_item, expYearItem);
		adapterMonth = new SpinnerAdapter(getActivity(),
				R.layout.layout_spinner_item, monthItem);

		cardType.setAdapter(adapterCType);
		sExpYear.setAdapter(adapterExpYear);
		sEMonthYear.setAdapter(adapterMonth);

		rootView.findViewById(R.id.btn_header_back).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						getActivity().onBackPressed();

					}
				});

		return rootView;
	}

	private void addTextChangeLisner() {

		// cvcNUmber
		// .setOnEditorActionListener(new TextView.OnEditorActionListener() {
		// @Override
		// public boolean onEditorAction(TextView v, int actionId,
		// KeyEvent event) {
		// if ((actionId == EditorInfo.IME_ACTION_DONE)) {
		// if (checkField()) {
		// hideKeyboard(getActivity(), cvcNUmber);
		// getStripeToken();
		//
		// } else {
		// hideKeyboard(getActivity(), cvcNUmber);
		// }
		// return true;
		// }
		// return false;
		// }
		// });

	}

	protected void getStripeToken() {
		UI.showProgressDialog(getActivity());
		Card card = new Card(cardNumber.getText().toString(), cardExpiryMonth,
				cardExpiryYear, cvcNUmber.getText().toString());

		boolean validation = card.validateCard();
		if (validation) {
			new Stripe().createToken(card, PUBLISHABLE_KEY,
					new TokenCallback() {
						public void onSuccess(Token token) {
							// LogMsg.LOG(getActivity(), token.toString());
							Log.e("TOKEN", token.toString());
							sendTokenToServer(token);

						}

						public void onError(Exception error) {
							// LogMsg.LOG(getActivity(),
							// error.getLocalizedMessage());
							Log.e("TOKEN_Error", error.getLocalizedMessage());
							UI.hideProgressDialog();
						}
					});
		} else if (!card.validateNumber()) {
			UI.hideProgressDialog();
			LogMsg.LOG(getActivity(),
					"The card number that you entered is invalid");
		} else if (!card.validateExpiryDate()) {
			UI.hideProgressDialog();
			LogMsg.LOG(getActivity(),
					"The expiration date that you entered is invalid");
		} else if (!card.validateCVC()) {
			UI.hideProgressDialog();
			LogMsg.LOG(getActivity(),
					"The CVC code that you entered is invalid");
		} else {
			UI.hideProgressDialog();
			LogMsg.LOG(getActivity(),
					"The card details that you entered are invalid");
		}

	}

	protected void sendTokenToServer(Token token) {

		if (token != null) {
			makeRequestForCharge(WSConstant.Web.CHARGE, token.getId());
		} else {
			LogMsg.LOG(getActivity(), "Error on getting card detail");
			UI.hideProgressDialog();
		}

	}

	// network call to purchase plan sending token to server
	private void makeRequestForCharge(String api, final String tokenId) {

		String url = Web.HOST + api;
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						parseResult(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {

				Map<String, String> params = new HashMap<String, String>();
				params.put(DataKey.TOKEN_ID, tokenId);
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	protected void parseResult(String response) {
		UI.hideProgressDialog();
		if (response != null) {
			try {
				JSONObject jsonOBject = new JSONObject(response);
				if (jsonOBject.getBoolean("success")) {
					Editor editor = sharedpreferences.edit();
					editor.putString(BiteBc.USER_PLAN, BiteBc.PLAN_ONE);
					editor.commit();

					Intent intent = new Intent(getActivity(),
							HomeActivity.class);
					startActivity(intent);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
					getActivity().finish();
				} else {
					LogMsg.LOG(getActivity(), "Failed To Registered");
				}

			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		} else {
			LogMsg.LOG(getActivity(), "Failed To Registered");
		}

	}

	private String getPublishKeyFromStripe() {
		String key = BiteBc.STRIPE_KEY;

		if (key.equals(BiteBc.STRIPE_TEST)) {
			return getString(R.string.pk_test);
		} else if (key.equals(BiteBc.STRIPE_TEST)) {
			return getString(R.string.pk_test);
		}
		return null;

	}

	// check ui field to be not null
	protected boolean checkField() {

		if (cardNumber.getText().toString().trim().equals("")) {
			LogMsg.LOG(getActivity(), "Please enter card number");
			cardNumber.requestFocus();
			cardNumber.setFocusableInTouchMode(true);
			return false;
		}
		if (cvcNUmber.getText().toString().trim().equals("")) {
			LogMsg.LOG(getActivity(), "Please enter Weight");
			cvcNUmber.requestFocus();
			cvcNUmber.setFocusableInTouchMode(true);
			return false;
		}

		return true;
	}

	// Class util funcitons to hide keyboard programically
	public static boolean hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

	}

	// handle all spinner listener here
	private void addSpinnerItemListner() {
		sExpYear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				cardExpiryYear = Integer.valueOf(parent.getItemAtPosition(
						position).toString());

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		});

		sEMonthYear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				cardExpiryMonth = (position + 1);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		});

	}

	private class SpinnerAdapter extends ArrayAdapter<String> {
		Context context;
		String[] items = new String[] {};

		public SpinnerAdapter(final Context context,
				final int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
			this.items = objects;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						R.layout.layout_spinner_dropdown, parent, false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.txt_d_id);
			tv.setText(items[position]);
			tv.setTextColor(Color.BLACK);
			tv.setTextSize(20);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.layout_spinner_item,
						parent, false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.txt_id);
			tv.setText(items[position]);
			tv.setTextColor(Color.BLACK);
			tv.setTextSize(20);
			return convertView;
		}

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_submit:
			if (checkField()) {
				hideKeyboard(getActivity(), cvcNUmber);
				getStripeToken();

			} else {
				hideKeyboard(getActivity(), cvcNUmber);
			}

			break;

		default:
			break;
		}

	}

}
