package com.brst.android.bite.app.membership;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.constant.WSConstant;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.UserDataHandler;
import com.brst.android.bite.app.domain.User;
import com.brst.android.bite.app.home.restaurant.ReviewScreenSliderFragment;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;

public class MemberShipStepOneFragment extends Fragment implements
		OnClickListener {
	Button btnPlaceOrder;

	private static String TAG = "MemberShipStepOneFragment";

	public static final String ARG_PLAN_ID = "id";
	public static final String ARG_NAME = "name";
	public static final String ARG_PRICE = "price";
	public static final String ARG_SUB_TOTAL = "subTotal";

	// <!-- totalValue subTotalPrice priceTotal planTimePeriod planName planName
	// -->

	TextView textUserName;
	TextView textEmail;

	TextView txtPlanName, txtplanPrice, planTimePeriod, txtsubTotalPrice,
			totalValue;
	UserDataHandler uDataHandler;

	User user;

	String planID, planName, planPrice, subTotalPrice;

	public static MemberShipStepOneFragment create(String id, String name,
			String price, String subTotal) {
		MemberShipStepOneFragment fragment = new MemberShipStepOneFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PLAN_ID, id);
		args.putString(ARG_NAME, name);
		args.putString(ARG_PRICE, price);
		args.putString(ARG_SUB_TOTAL, subTotal);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		planID = getArguments().getString(ARG_PLAN_ID);
		planName = getArguments().getString(ARG_NAME);
		planPrice = getArguments().getString(ARG_PRICE);
		subTotalPrice = getArguments().getString(ARG_SUB_TOTAL);
		uDataHandler = UserDataHandler.getInstance();
		user = uDataHandler.getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_step_one,
				container, false);

		btnPlaceOrder = (Button) rootView.findViewById(R.id.btn_place_order);
		btnPlaceOrder.setOnClickListener(this);

		textUserName = (TextView) rootView.findViewById(R.id.text_user_name);
		textEmail = (TextView) rootView.findViewById(R.id.text_email);
		textUserName.setText(user.getName());
		textEmail.setText(user.getEmail());

		txtPlanName = (TextView) rootView.findViewById(R.id.planName);
		txtPlanName.setText(planName);
		txtplanPrice = (TextView) rootView.findViewById(R.id.planPrice);
		txtplanPrice.setText(planPrice);
		planTimePeriod = (TextView) rootView.findViewById(R.id.planTimePeriod);
		String time = (!planID.equals("5")) ? planID.equals("4") ? "6 Month"
				: "1 month" : "12 Month";
		planTimePeriod.setText(time);
		txtsubTotalPrice = (TextView) rootView.findViewById(R.id.subTotalPrice);
		txtsubTotalPrice.setText(subTotalPrice);
		totalValue = (TextView) rootView.findViewById(R.id.totalValue);
		totalValue.setText(subTotalPrice);

		rootView.findViewById(R.id.btn_header_back).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						getActivity().onBackPressed();

					}
				});

		return rootView;
	}

	private void makeRequestToBuyPlan(String api, final String planId,
			final String custId) {

		String url = Web.HOST + api;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						parsePlans(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {

				Map<String, String> params = new HashMap<String, String>();
				params.put(DataKey.PLANID, planId);
				params.put(DataKey.CUST_ID, custId);
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	protected void parsePlans(String response) {

		UI.hideProgressDialog();

		try {
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				LogMsg.LOG(getActivity(), "Succesfully Registered");
				FragmentManager fm = this.getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, new MemberShipStepTwoFragment());
				ft.addToBackStack(null);
				ft.commit();

			} else {
				LogMsg.LOG(getActivity(), "Failed To Registered");
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_place_order:
			Log.e(TAG, planID + "AND User id:" + user.getCustomerid());
			makeRequestToBuyPlan(WSConstant.Web.BUY, planID,
					user.getCustomerid());
			break;

		default:
			break;
		}

	}
}
