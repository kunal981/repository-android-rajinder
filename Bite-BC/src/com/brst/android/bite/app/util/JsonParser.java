package com.brst.android.bite.app.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.brst.android.bite.app.domain.Plan;
import com.brst.android.bite.app.domain.Rating;
import com.brst.android.bite.app.domain.Restaurant;

public class JsonParser {

	private static final String TAG = "JSONParser handler";

	public static List<Restaurant> getRestaurantsDetail(String strJson) {

		List<Restaurant> listRestaurants = null;
		try {
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				listRestaurants = new ArrayList<Restaurant>();

				JSONArray jArray = jsonOBject.getJSONArray("products");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectProduct = jArray.getJSONObject(i);
					Restaurant res = new Restaurant();
					res.setRating(jsonOBjectProduct.getInt("rating"));
					JSONObject jsonObjectDetail = jsonOBjectProduct
							.getJSONObject("detail");
					res.setName(jsonObjectDetail.getString("name").toUpperCase(
							Locale.getDefault()));
					res.setId(jsonObjectDetail.getString("entity_id"));
					res.setDescription(jsonObjectDetail
							.getString("description"));
					res.setAddress(jsonObjectDetail.getString("address"));
					res.setCuisine(jsonObjectDetail.getString("cuisine").trim());
					if (!jsonObjectDetail.isNull("offer_available")
							&& jsonObjectDetail.getString("offer_available") != null) {
						res.setOfferAvailable(jsonObjectDetail
								.getString("offer_available"));
					}
					if (!jsonObjectDetail.isNull("latitude")) {
						res.setLatitude(String.valueOf(jsonObjectDetail
								.getDouble("latitude")));
					}
					if (!jsonObjectDetail.isNull("longitude")) {
						res.setLongitude(String.valueOf(jsonObjectDetail
								.getDouble("longitude")));
					}

					if (!jsonObjectDetail.isNull("offer_period")) {
						res.setOfferPeriod(jsonObjectDetail
								.getString("offer_period"));
					}
					res.setImage(jsonObjectDetail.getString("image_url"));
					if (!jsonObjectDetail.isNull("website_link")
							&& jsonObjectDetail.getString("website_link") != null)
						res.setWebUrl(jsonObjectDetail
								.getString("website_link"));

					if (!jsonObjectDetail.isNull("number_people")
							&& jsonObjectDetail.getString("number_people") != null)
						res.setPeople(jsonObjectDetail
								.getString("number_people"));
					if (!jsonObjectDetail.isNull("zipcode"))
						res.setZipcode(jsonObjectDetail.getString("zipcode") != null ? jsonObjectDetail
								.getString("zipcode").trim() : "");
					if (!jsonObjectDetail.isNull("num_of_people")
							&& jsonObjectDetail.getString("num_of_people") != null)
						res.setPeoplePerBook(jsonObjectDetail.getString(
								"num_of_people").trim());
					if (!jsonObjectDetail.isNull("monthly")
							&& !jsonObjectDetail.getString("monthly").trim()
									.equals(""))
						res.setMonthNoOffer(AppUtility
								.parseMonthData(jsonObjectDetail.getString(
										"monthly").trim()));
					if (!jsonObjectDetail.isNull("phone_number")
							&& jsonObjectDetail.getString("phone_number") != null)
						res.setPhoneNo(jsonObjectDetail
								.getString("phone_number"));
					if (!jsonObjectDetail.isNull("booking_number")
							&& jsonObjectDetail.getString("booking_number") != null)
						res.setBookingNo(jsonObjectDetail
								.getString("booking_number"));
					if (!jsonObjectDetail.isNull("cellphone")
							&& jsonObjectDetail.getString("cellphone") != null)
						res.setCellphone((jsonObjectDetail
								.getString("cellphone").toUpperCase(Locale
								.getDefault())));
					if (!jsonObjectDetail.isNull("weekly")
							&& jsonObjectDetail.getJSONArray("weekly") != null) {
						JSONArray jWeekArray = jsonObjectDetail
								.getJSONArray("weekly");
						List<String> listWeeks = new ArrayList<String>();
						for (int j = 0; j < jWeekArray.length(); j++) {
							String element = jWeekArray.getString(j);
							listWeeks.add(AppUtility.parseWeekData(element));
						}
						res.setListWeek(listWeeks);

					}

					listRestaurants.add(res);
				}

			} else {
				listRestaurants = new ArrayList<Restaurant>();
			}

		} catch (JSONException e) {
			listRestaurants = new ArrayList<Restaurant>();
			Log.e(TAG, e.getMessage());
		}

		return listRestaurants;

	}

	public static List<String> getRestaurantGallery(JSONObject jsonProductobject)
			throws JSONException {
		List<String> imagesList = new ArrayList<String>();
		if (!jsonProductobject.isNull("images")) {
			JSONArray jsonImagesArray = jsonProductobject
					.getJSONArray("images");
			for (int i = 0; i < jsonImagesArray.length(); i++) {
				imagesList.add(jsonImagesArray.getString(i));

			}
		}

		return imagesList;

	}

	public static List<Rating> getRestarantRating(JSONObject jsonProductobject)
			throws JSONException {
		List<Rating> ratingItems = new ArrayList<Rating>();
		if (!jsonProductobject.isNull("allratings")) {
			JSONArray jsonRatingArray = jsonProductobject
					.getJSONArray("allratings");

			for (int i = 0; i < jsonRatingArray.length(); i++) {
				Rating r = new Rating();
				JSONObject jsonObjectRating = jsonRatingArray.getJSONObject(i);

				r.setRatingBy(jsonObjectRating.getString("name"));
				r.setQuotes(jsonObjectRating.getString("review_detail"));
				r.setRatingValue(jsonObjectRating.getInt("rating"));
				r.setRatingDate(jsonObjectRating.getString("date"));
				ratingItems.add(r);
			}
		}

		return ratingItems;

	}

	public static Restaurant getRestaurant(String strJson, Restaurant restaurant) {

		Restaurant restaurantLocal = restaurant;
		JSONObject jsonOBject;
		List<Rating> ratingItems = new ArrayList<Rating>();
		List<String> imagesList = new ArrayList<String>();

		try {
			jsonOBject = new JSONObject(strJson);

			if (jsonOBject.getBoolean("success")) {

				JSONObject jsonProductobject = jsonOBject
						.getJSONObject("product_detail");
				ratingItems = getRestarantRating(jsonProductobject);
				imagesList = getRestaurantGallery(jsonProductobject);
				restaurant.setRatings(ratingItems);
				restaurant.setGalleryImages(imagesList);

			} else {
				restaurantLocal.setRatings(ratingItems);
				restaurantLocal.setGalleryImages(imagesList);
			}
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}

		return restaurantLocal;

	}

	public static List<Plan> getPlanForNewUsers(String strJson) {

		Plan plan;
		JSONObject jsonOBject;
		List<Plan> listPlans = new ArrayList<Plan>();

		try {
			jsonOBject = new JSONObject(strJson);

			if (jsonOBject.getBoolean("success")) {
				JSONArray jsonPlansArray = null;
				if (!jsonOBject.isNull("products")) {
					jsonPlansArray = jsonOBject.getJSONArray("products");
				}

				if (jsonPlansArray != null) {

					for (int i = 0; i < jsonPlansArray.length(); i++) {

						JSONObject jsonPlan = jsonPlansArray.getJSONObject(i);
						plan = new Plan();
						plan.setPlanName(jsonPlan.getString("name"));
						plan.setPlanId(jsonPlan.getString("id"));
						plan.setPrice(jsonPlan.getString("price"));
						plan.setSubTotalprice(jsonPlan.getString("subtotal"));

						listPlans.add(plan);
					}

				}

			}
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}

		return listPlans;

	}
}
