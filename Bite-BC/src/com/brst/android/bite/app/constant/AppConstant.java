package com.brst.android.bite.app.constant;

public class AppConstant {

	public static class BiteBc {
		public static final String MyPREFERENCES = "MyBiteBC";
		public static final String USER_PLAN = "uBitePlan";
		public static final String USER_ID = "uID";
		public static final String USER_PWD = "uPWD";
		public static final String PLAN_SIX = "6";
		public static final String PLAN_ONE = "1";
		public static final String PLAN_YEAR = "12";

		public static final String STRIPE_TEST = "test";
		public static final String STRIPE_LIVE = "live";
		public static final String STRIPE_KEY = STRIPE_TEST;    // chnge this to STRIPE_LIVE when productiuon support is needed

	}

}
