package com.brst.android.bite.app.constant;

public class WSConstant {

	public static class Web {
		public static final String HOST = "http://bitebc.ca/api/api/";
		public static final String LOGIN = "login.php";
		public static final String REGISTER = "register.php";
		public static final String PRODUCT = "products.php";
		public static final String SEARCH = "product.php";
		public static final String FILTER = "filter.php";
		public static final String REVIEW = "review.php";
		public static final String NEAR_BY = "nearby.php";
		public static final String CHECKIN = "checkin.php";
		public static final String CHECK_IN = "check_in.php";
		public static final String PLANS = "plans.php";
		public static final String BUY = "payment.php";
		public static final String NEAR_ME = "nearme.php";
		public static final String CHARGE = "charge.php";
	}

	// public static final String HOST="http://bitebc.ca/api/api/";
	// public static final String HOST="http://bitebc.ca/api/api/";
	// public static final String HOST="http://bitebc.ca/api/api/";
	// public static final String HOST="http://bitebc.ca/api/api/";

	public static class DataKey {

		public static final String TAG_JSON_OBJECT = "jobj_req";
		public static final String DATA_SEARCH = "datasearch";
		public static final String PRODUCT_KEY = "productid";
		public static final String CUSTOMER_ID = "coustomerid";
		public static final String LAT = "lat";
		public static final String LONG = "lang";
		public static final String PLANID = "planid";
		public static final String CUST_ID = "customerid";
		public static final String REST_ID = "rest_id";
		public static final String USER_ID = "email";
		public static final String USER_PWD = "password";
		public static final String TOKEN_ID = "stripeToken";
		
	}

}
