package com.brst.android.bite.app.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.base.BaseContainerFragment;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.domain.Restaurant;
import com.brst.android.bite.app.util.JsonParser;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;

public class Home extends Fragment implements OnClickListener,
		OnItemClickListener {

	public static final int FRAGMENT_CODE = 328;
	private RestaurantDataHandler rHandler;

	List<Restaurant> listItems;
	private static final String TAG = "Home";

	// private SearchView mSearchView;
	ListView listRestaurant;
	RestuarantListAdapter adapter;
	Button btnFilter;
	ImageView refresh;

	private ProgressDialog pDialog;
	// These tags will be used to cancel the requests
	private String tag_json_obj = "jobj_req";

	ImageLoader imageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = AppController.getInstance().getImageLoader();
		rHandler = RestaurantDataHandler.getInstance();

		listItems = new ArrayList<Restaurant>();

		makeRequestForRestaurant(Web.PRODUCT);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home_rest_listing,
				container, false);

		// mSearchView = (SearchView) rootView.findViewById(R.id.search_view);
		listRestaurant = (ListView) rootView.findViewById(R.id.list_view);

		btnFilter = (Button) rootView.findViewById(R.id.btn_filter);
		btnFilter.setOnClickListener(this);

		refresh = (ImageView) rootView.findViewById(R.id.refresh);
		refresh.setOnClickListener(this);
		// intiProgressDailog();
		// setupSearchView();
		adapter = new RestuarantListAdapter(getActivity(), listItems);
		listRestaurant.setAdapter(adapter);
		listRestaurant.setOnItemClickListener(this);

		return rootView;

	}

	@Override
	public void onResume() {
		super.onResume();
		// if (listItems.size() == 0) {
		// makeRequestForRestaurant(WSConstant.PRODUCT);
		// // makeRequestForFilter(WSConstant.FILTER);
		//
		// }

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FRAGMENT_CODE && resultCode == Activity.RESULT_OK) {
			if (data != null) {
				String value = data.getStringExtra("people");
				String cusine = data.getStringExtra("cusine");
				String offer1 = data.getStringExtra("offer1");
				String availbilty = data.getStringExtra("availbity");
				// String offer2 = data.getStringExtra("offer2");
				if (value != null || cusine != null || offer1 != null) {
					Log.e(TAG, "Data passed from Child fragment = " + value
							+ cusine);

					HashMap<String, String> param = new HashMap<String, String>();
					param.put("cuisine_types[]", cusine);
					param.put("people[]", value);
					param.put("offer_type[]", offer1);
					param.put("availability[]", availbilty);

					makeRequestForFilter(Web.FILTER, param);

				} else {
					Toast.makeText(getActivity(), "No search availble",
							Toast.LENGTH_LONG).show();
				}
			}
		}
	}

	class RestuarantListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		List<Restaurant> listItems;

		public RestuarantListAdapter(Activity activity,
				List<Restaurant> listsItems) {
			this.activity = activity;
			this.listItems = listsItems;
		}

		public void addItems(List<Restaurant> listItems) {
			this.listItems = listItems;
			notifyDataSetChanged();

		}

		@Override
		public int getCount() {

			return listItems.size();
		}

		@Override
		public Object getItem(int position) {

			return listItems.get(position);
		}

		@Override
		public long getItemId(int id) {

			return id;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			final ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.layout_list_restaurant_item, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.text_r_name);
				holder.cusine = (TextView) convertView
						.findViewById(R.id.text_r_c_type);

				holder.image = (ImageView) convertView
						.findViewById(R.id.image_id);
				// holder.iconAdd.setImageResource(R.drawable.btn_plus);
				holder.offer = (TextView) convertView
						.findViewById(R.id.discount);
				holder.rate = (RatingBar) convertView
						.findViewById(R.id.rating_rest);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final Restaurant item = listItems.get(position);
			holder.name.setText(item.getName());
			holder.cusine.setText(item.getCuisine());
			holder.offer
					.setText(!item.getOfferAvailable().equals("No Offers") ? item
							.getOfferAvailable().equals("31") ? "2 for 1 meal"
							: "50% off food" : item.getOfferAvailable());
			holder.rate.setProgress(item.getRating());
			holder.name.setText(item.getName());

			// If you are using normal ImageView
			imageLoader.get(item.getImage(), new ImageListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e(TAG, "Image Load Error: " + error.getMessage());
				}

				@Override
				public void onResponse(ImageContainer response, boolean arg1) {
					if (response.getBitmap() != null) {
						// load image into imageview
						holder.image.setImageBitmap(response.getBitmap());
					}
				}
			});

			return convertView;
		}
	}

	static class ViewHolder {
		TextView name;
		TextView cusine;
		ImageView image;
		TextView offer;
		RatingBar rate;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_filter:
			Filter fragment = new Filter();
			fragment.setTargetFragment(this, FRAGMENT_CODE);
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					fragment, true);
			break;
		case R.id.refresh:
			listItems = new ArrayList<Restaurant>();
			makeRequestForRestaurant(Web.PRODUCT);
			break;

		default:
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Restaurant item = listItems.get(position);
		rHandler.setRestaurant(item);

		RestaurantDetailMain fragment = new RestaurantDetailMain();
		((BaseContainerFragment) getParentFragment()).replaceFragment(fragment,
				true);

	}

	private void makeRequestForRestaurant(String product) {

		String url = Web.HOST + product;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());
						parseResturant(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				});

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

	}

	protected void parseResturant(String response) {
		listItems = JsonParser.getRestaurantsDetail(response);

		if (listItems != null && listItems.size() != 0) {
			adapter.addItems(listItems);
		} else {
			LogMsg.LOG(getActivity(), "No search Availalbe");
			adapter.addItems(listItems);

		}
		UI.hideProgressDialog();

	}

	private void makeRequestForFilter(String register,
			final HashMap<String, String> param) {

		String url = Web.HOST + register;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());

						parseResturant(response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			/**
			 * Passing some request headers
			 * */
			// @Override
			// public Map<String, String> getHeaders() throws AuthFailureError {
			// HashMap<String, String> headers = new HashMap<String, String>();
			// headers.put("Content-Type", "application/json");
			// return headers;
			// }

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = param;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	// private void showProgressDialog() {
	// if (pDialog == null) {
	// pDialog = new ProgressDialog(getActivity());
	// pDialog.setMessage("Loading...");
	// pDialog.setCancelable(false);
	// pDialog.show();
	// }
	// }
	//
	// private void hideProgressDialog() {
	// if (pDialog.isShowing() && pDialog != null) {
	// pDialog.dismiss();
	// pDialog = null;
	// }
	// }
}
