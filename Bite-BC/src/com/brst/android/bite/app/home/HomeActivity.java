package com.brst.android.bite.app.home;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.brst.android.bite.app.R;
import com.brst.android.bite.app.base.BaseContainerFragment;
import com.brst.android.bite.app.base.ActivityFragmentContainer;
import com.brst.android.bite.app.base.NearMeFragmentContainer;
import com.brst.android.bite.app.base.PlaceHolderContainerFragment;
import com.brst.android.bite.app.base.PlaceholderFragment;
import com.brst.android.bite.app.base.SearchFragmentContainer;
import com.brst.android.bite.app.base.SettingFragmentContainer;

public class HomeActivity extends FragmentActivity {
	private FragmentTabHost mTabHost;

	private static final String TAB_ACTIVITY = "Restaurants";
	private static final String TAB_NEAR_ME = "Near Me";
	private static final String TAB_SEARCH = "Search";
	private static final String TAB_SETTING = "Setting";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_ACTIVITY),
						R.drawable.home, R.drawable.tab_item_selector),
				ActivityFragmentContainer.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_NEAR_ME),
						R.drawable.near_me, R.drawable.tab_item_selector),
				NearMeFragmentContainer.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_SEARCH),
						R.drawable.search, R.drawable.tab_item_selector),
				SearchFragmentContainer.class, null);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_SETTING),
						R.drawable.settings, R.drawable.tab_item_selector),
				SettingFragmentContainer.class, null);
	}

	public TabSpec setIndicator(TabSpec spec, int resid, int tabId) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
		v.setBackgroundResource(tabId);
		ImageView imageButton = (ImageView) v.findViewById(R.id.image_tabs);
		imageButton.setImageResource(resid);
		TextView text = (TextView) v.findViewById(R.id.text_tabs);
		// text.setTextColor(R.drawable.text_color_picker);
		text.setText(spec.getTag());

		return spec.setIndicator(v);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		boolean isPopFragment = false;
		String currentTabTag = mTabHost.getCurrentTabTag();

		if (currentTabTag.equals(TAB_ACTIVITY)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_ACTIVITY)).popFragment();
		} else if (currentTabTag.equals(TAB_NEAR_ME)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_NEAR_ME)).popFragment();
		} else if (currentTabTag.equals(TAB_SEARCH)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_SEARCH)).popFragment();
		} else if (currentTabTag.equals(TAB_SETTING)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_SETTING)).popFragment();
		}

		if (!isPopFragment) {

			if (mTabHost.getCurrentTab() == 0) {
				finish();
			} else {
				mTabHost.setCurrentTab(0);
			}
		}
	}

	public void onBackButtonClick(View v) {
		this.onBackPressed();
	}

}
