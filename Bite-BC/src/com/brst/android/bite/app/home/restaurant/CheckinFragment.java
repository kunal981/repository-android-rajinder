package com.brst.android.bite.app.home.restaurant;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.control.UserDataHandler;
import com.brst.android.bite.app.domain.Restaurant;
import com.brst.android.bite.app.domain.User;
import com.brst.android.bite.app.util.LogMsg;
import com.brst.android.bite.app.util.UI;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

public class CheckinFragment extends Fragment implements OnClickListener {

	private static String TAG = "CheckinFragment";
	RestaurantDataHandler rHandler;
	Restaurant restaurant;
	UserDataHandler uHandler;
	User user;
	ImageLoader imageLoader;
	private boolean isResumed = false;

	TextView name;
	Button btnConfrim;
	ProfilePictureView profilePictureView;
	String userImageurl;

	TextView validity, remainingTime;

	String packageDescription;
	String daysLeft;
	HashMap<String, String> params;

	private static final int REAUTH_ACTIVITY_CODE = 100;

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback); // track the

		uiHelper.onCreate(savedInstanceState);
		uHandler = UserDataHandler.getInstance();
		user = uHandler.getUser();
		rHandler = RestaurantDataHandler.getInstance();
		restaurant = rHandler.getRestaurant();
		// imageLoader = AppController.getInstance().getImageLoader();
		// userImageurl = "http://graph.facebook.com/" + user.getuFacebookId()
		// + "/picture?type=small";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fragment_layout_checkin, null);

		name = (TextView) rootView.findViewById(R.id.name);
		btnConfrim = (Button) rootView.findViewById(R.id.btn_confirm);
		btnConfrim.setOnClickListener(this);

		profilePictureView = (ProfilePictureView) rootView
				.findViewById(R.id.selection_profile_pic);
		profilePictureView.setCropped(true);
		name.setText(user.getName());

		validity = (TextView) rootView.findViewById(R.id.validity);
		remainingTime = (TextView) rootView.findViewById(R.id.remainingTime);

		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			// Get the user's data
			makeMeRequest(session);
		}
		params = new HashMap<String, String>();
		params.put(DataKey.CUSTOMER_ID, user.getCustomerid());
		makeRequestForCheckIn(Web.CHECKIN, params);

		return rootView;
	}

	/**
	 * service to fetch checking detail
	 * 
	 */

	private void makeRequestForCheckIn(final String checkin,
			final Map<String, String> params) {

		String url = Web.HOST + checkin;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());
						if (checkin.equals(Web.CHECKIN)) {
							parseResponse(response);
						} else if (checkin.equals(Web.CHECK_IN)) {
							parseResponseForCheckIn(response);
						}
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {

				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	protected void parseResponseForCheckIn(String response) {

		try {
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				if (jsonOBject.getString("status").equals("true")) {
					LogMsg.LOG(getActivity(), "Sucessfully Checked In");
					btnConfrim.setVisibility(View.GONE);
				} else {
					LogMsg.LOG(getActivity(), "Failed to Checked In");
				}
			} else {
				packageDescription = "No membership Plan found";
				daysLeft = "";
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}
		UI.hideProgressDialog();

	}

	protected void parseResponse(String response) {

		try {
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				packageDescription = jsonOBject.getString("package_name");
				daysLeft = String.valueOf(jsonOBject.getInt("daysleft"));
			} else {
				packageDescription = "No membership Plan found";
				daysLeft = "";
			}

		} catch (JSONException e) {
			packageDescription = "Sorry no information";
			daysLeft = "";
			Log.e(TAG, e.getMessage());
		}

		validity.setText(packageDescription);
		if (!daysLeft.equals("")) {
			remainingTime.setText(daysLeft + " Days left");
		} else {
			remainingTime.setVisibility(View.GONE);
		}
		UI.hideProgressDialog();

	}

	/**
	 * Facebook initialization
	 * 
	 * @param session
	 * @param state
	 * @param exception
	 */

	private void onSessionStateChange(final Session session,
			SessionState state, Exception exception) {
		if (session != null && session.isOpened()) {
			// Get the user's data.
			makeMeRequest(session);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		uiHelper.onSaveInstanceState(bundle);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REAUTH_ACTIVITY_CODE) {
			uiHelper.onActivityResult(requestCode, resultCode, data);
		}
	}

	private void makeMeRequest(final Session session) {
		// Make an API call to get user data and define a
		// new callback to handle the response.
		Request request = Request.newMeRequest(session,
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						// If the response is successful
						if (session == Session.getActiveSession()) {
							if (user != null) {
								// Set the id for the ProfilePictureView
								// view that in turn displays the profile
								// picture.
								profilePictureView.setProfileId(user.getId());
								// Set the Textview's text to the user's name.
							}
						}
						if (response.getError() != null) {
							profilePictureView.setProfileId(null); // Handle
						}
					}
				});
		request.executeAsync();
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_confirm:
			params = new HashMap<String, String>();
			params.put(DataKey.CUST_ID, user.getCustomerid());
			params.put(DataKey.REST_ID, restaurant.getId());
			makeRequestForCheckIn(Web.CHECK_IN, params);
			break;

		default:
			break;
		}

	}

}
