package com.brst.android.bite.app.home;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.StringRequest;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.base.BaseContainerFragment;
import com.brst.android.bite.app.constant.WSConstant.DataKey;
import com.brst.android.bite.app.constant.WSConstant.Web;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.domain.Restaurant;
import com.brst.android.bite.app.home.restaurant.CheckinFragment;
import com.brst.android.bite.app.home.restaurant.Detail;
import com.brst.android.bite.app.home.restaurant.Gallery;
import com.brst.android.bite.app.home.restaurant.Reviews;
import com.brst.android.bite.app.locate.MyMapFragemt;
import com.brst.android.bite.app.util.JsonParser;
import com.brst.android.bite.app.util.UI;

public class RestaurantDetailMain extends Fragment implements OnClickListener {

	private static final String TAG = "RestaurantDetail";
	Button btnDetail, btnGallery, btnReview, btnCheckin;
	ImageView map;

	ImageView imageRestaurant;

	TextView name, address;

	RestaurantDataHandler rHandler;
	Restaurant restaurant;
	ImageLoader imageLoader;

	// These tags will be used to cancel the requests

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		imageLoader = AppController.getInstance().getImageLoader();
		rHandler = RestaurantDataHandler.getInstance();
		restaurant = rHandler.getRestaurant();

		makeRequestForRestaurantData(Web.PRODUCT, restaurant.getId());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragment_layout_restaurant_detail, null);

		name = (TextView) rootView.findViewById(R.id.header_title);

		address = (TextView) rootView.findViewById(R.id.text_adr1);

		imageRestaurant = (ImageView) rootView.findViewById(R.id.image_hotel);

		btnDetail = (Button) rootView.findViewById(R.id.btn_detail);
		btnGallery = (Button) rootView.findViewById(R.id.btn_gallery);
		btnReview = (Button) rootView.findViewById(R.id.btn_review);
		btnCheckin = (Button) rootView.findViewById(R.id.btn_checkin);
		// back = (ImageView) rootView.findViewById(R.id.btn_header_back);
		map = (ImageView) rootView.findViewById(R.id.btn_map);
		// back.setOnClickListener(this);
		map.setOnClickListener(this);
		btnDetail.setOnClickListener(this);
		btnGallery.setOnClickListener(this);
		btnReview.setOnClickListener(this);
		btnCheckin.setOnClickListener(this);

		btnDetail.setSelected(true);

		updateUiValues();

		return rootView;

	}

	@Override
	public void onResume() {
		super.onResume();

	}

	public void updateUiValues() {
		name.setText(restaurant.getName());
		address.setText(restaurant.getAddress());
		imageLoader.get(restaurant.getImage(), new ImageListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "Image Load Error: " + error.getMessage());
			}

			@Override
			public void onResponse(ImageContainer response, boolean arg1) {
				if (response.getBitmap() != null) {
					// load image into imageview
					imageRestaurant.setImageBitmap(response.getBitmap());
				}
			}
		});
		getChildFragmentManager().beginTransaction()
				.replace(R.id.restarant_detail_frame_container, new Detail())
				.commit();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		switch (id) {
		case R.id.btn_detail:
			if (!btnDetail.isSelected()) {
				btnDetail.setSelected(true);
				btnGallery.setSelected(false);
				btnReview.setSelected(false);

				getChildFragmentManager()
						.beginTransaction()
						.replace(R.id.restarant_detail_frame_container,
								new Detail()).commit();

			}

			break;
		case R.id.btn_gallery:
			if (!btnGallery.isSelected()) {
				btnDetail.setSelected(false);
				btnGallery.setSelected(true);
				btnReview.setSelected(false);

				getChildFragmentManager()
						.beginTransaction()
						.replace(R.id.restarant_detail_frame_container,
								new Gallery()).commit();

			}
			break;
		case R.id.btn_review:
			if (!btnReview.isSelected()) {
				btnDetail.setSelected(false);
				btnGallery.setSelected(false);
				btnReview.setSelected(true);

				getChildFragmentManager()
						.beginTransaction()
						.replace(R.id.restarant_detail_frame_container,
								new Reviews()).commit();
			}
			break;
		case R.id.btn_checkin:
			CheckinFragment fragment = new CheckinFragment();
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					fragment, true);
			break;
		case R.id.btn_map:
			MyMapFragemt map = MyMapFragemt.create(restaurant.getId());
			((BaseContainerFragment) getParentFragment()).replaceFragment(map,
					true);

			break;

		default:
			break;
		}

	}

	private void makeRequestForRestaurantData(String product, final String id) {

		String url = Web.HOST + product;
		UI.showProgressDialog(getActivity());
		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.e(TAG + "On Response", response);
						// msgResponse.setText(response.toString());
						saveRestaurantData(response);
					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UI.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {

				Map<String, String> params = new HashMap<String, String>();
				params.put(DataKey.PRODUCT_KEY, id);
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				DataKey.TAG_JSON_OBJECT);

	}

	protected void saveRestaurantData(String response) {
		UI.hideProgressDialog();

		restaurant = JsonParser.getRestaurant(response, restaurant);
		rHandler.setRestaurant(restaurant);

		updateUiValues();

	}

}
