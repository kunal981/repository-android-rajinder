package com.brst.android.bite.app.home.restaurant;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.brst.android.bite.app.R;
import com.brst.android.bite.app.control.AppController;
import com.brst.android.bite.app.control.RestaurantDataHandler;
import com.brst.android.bite.app.domain.Restaurant;

@SuppressLint("DefaultLocale")
public class Detail extends Fragment implements OnClickListener {

	private static final String TAG = "RestaurantDetail.Detail";
	private ArrayList<String> parentItems;
	private ArrayList<Object> childItems;

	ImageView imageHotel;
	TextView discount, restrictday, webUrl, restrictedMonth, restrictPeople,
			phoneNumber, textCall;

	RelativeLayout relativeLayoutDiscount, relativeLayoutTextPeople,
			relativeLayoutMonth, relativeLayoutCall;

	LinearLayout relativeLayoutWeekOne, relativeLayoutWeekTwo,
			relativeLayoutWeekThree;

	RatingBar rate;

	ExpandableListView expandableListView;
	MyExpandableAdapter adapter;

	RestaurantDataHandler rHandler;
	Restaurant restaurant;
	ImageLoader imageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = AppController.getInstance().getImageLoader();
		rHandler = RestaurantDataHandler.getInstance();
		restaurant = rHandler.getRestaurant();
	}

	/**
	 * image_hotel_2 text_r_discount text_day text_cuisine_info rating_bar
	 * text_web_url
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_detail, null);

		imageHotel = (ImageView) rootView.findViewById(R.id.image_hotel_2);
		relativeLayoutDiscount = (RelativeLayout) rootView
				.findViewById(R.id.layout_text_r_discount);
		discount = (TextView) rootView.findViewById(R.id.text_r_discount);
		// restrictday = (TextView) rootView.findViewById(R.id.text_day);
		// relativeLayoutWeekOffer = (RelativeLayout) rootView
		// .findViewById(R.id.layout_text_day);
		restrictedMonth = (TextView) rootView.findViewById(R.id.text_month);
		relativeLayoutMonth = (RelativeLayout) rootView
				.findViewById(R.id.layout_text_Month);
		restrictPeople = (TextView) rootView.findViewById(R.id.text_people);
		relativeLayoutTextPeople = (RelativeLayout) rootView
				.findViewById(R.id.layout_text_people);
		relativeLayoutCall = (RelativeLayout) rootView
				.findViewById(R.id.layout_text_call);
		relativeLayoutWeekOne = (LinearLayout) rootView
				.findViewById(R.id.layout_week_1);
		relativeLayoutWeekTwo = (LinearLayout) rootView
				.findViewById(R.id.layout_week_2);
		relativeLayoutWeekThree = (LinearLayout) rootView
				.findViewById(R.id.layout_week_3);

		textCall = (TextView) rootView.findViewById(R.id.text_call);

		webUrl = (TextView) rootView.findViewById(R.id.text_web_url);
		webUrl.setOnClickListener(this);
		rate = (RatingBar) rootView.findViewById(R.id.rating_bar);

		phoneNumber = (TextView) rootView.findViewById(R.id.text_phone_num);
		phoneNumber.setOnClickListener(this);

		loadData();

		expandableListView = (ExpandableListView) rootView
				.findViewById(R.id.list_exp_view);

		setUpGroupIndicator();
		// Set the Items of Parent
		setGroupParents();
		// Set The Child Data
		setChildData();
		adapter = new MyExpandableAdapter(getActivity(), parentItems,
				childItems);
		expandableListView.setAdapter(adapter);

		return rootView;
	}

	@SuppressLint("DefaultLocale")
	private void loadData() {
		// TODO Auto-generated method stub
		imageLoader.get(restaurant.getImage(), new ImageListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "Image Load Error: " + error.getMessage());
			}

			@Override
			public void onResponse(ImageContainer response, boolean arg1) {
				if (response.getBitmap() != null) {
					// load image into imageview
					imageHotel.setImageBitmap(response.getBitmap());
				}
			}
		});
		if (!restaurant.getOfferAvailable().equals("No Offers")) {
			discount.setText(restaurant.getOfferAvailable().equals("31") ? "2 for 1"
					: "50%");
		} else {
			relativeLayoutDiscount.setVisibility(View.GONE);
		}
		// if (!restaurant.getDayNoOffer().equals("")) {
		// restrictday.setText(restaurant.getDayNoOffer());
		// } else {
		// relativeLayoutWeekOffer.setVisibility(View.GONE);
		// }
		if (!restaurant.getMonthNoOffer().equals("")) {
			restrictedMonth.setText(restaurant.getMonthNoOffer());
		} else {
			relativeLayoutMonth.setVisibility(View.GONE);
		}
		if (!restaurant.getPeoplePerBook().equals("0")) {
			restrictPeople.setText("x" + restaurant.getPeoplePerBook());
		} else {
			relativeLayoutTextPeople.setVisibility(View.GONE);
		}

		if (restaurant.getCellphone().equals("YES")) {
			relativeLayoutCall.setVisibility(View.VISIBLE);
		} else {
			relativeLayoutCall.setVisibility(View.GONE);
		}

		if (restaurant.getPhoneNo() != null) {
			phoneNumber.setText(restaurant.getPhoneNo());
		} else {
			phoneNumber.setVisibility(View.GONE);
		}

		if (restaurant.getListWeek() != null
				&& restaurant.getListWeek().size() != 0) {
			List<String> weekElement = restaurant.getListWeek();
			if (weekElement.size() > 6) {
				relativeLayoutWeekThree.setVisibility(View.VISIBLE);
				relativeLayoutWeekTwo.setVisibility(View.VISIBLE);
			} else if (weekElement.size() > 4) {
				relativeLayoutWeekTwo.setVisibility(View.VISIBLE);
			}
			for (int i = 0; i < weekElement.size(); i++) {
				String element = weekElement.get(i);
				if (element.contains("NO")) {
					break;
				}
				relativeLayoutWeekOne.setVisibility(View.VISIBLE);
				View view = inflateView(element);
				if (i >= 0 && i <= 2) {
					relativeLayoutWeekOne.addView(view);
				} else if (i >= 3 && i <= 5) {
					relativeLayoutWeekTwo.addView(view);
				} else if (i == 6) {
					relativeLayoutWeekThree.addView(view);
				}

			}
		}

		webUrl.setText(restaurant.getWebUrl());
		rate.setProgress(restaurant.getRating());

	}

	private View inflateView(String element) {
		View view = getActivity().getLayoutInflater().inflate(
				R.layout.layout_week_info_item, null);

		TextView dayNoOffer = (TextView) view.findViewById(R.id.text_day);
		dayNoOffer.setText(element);

		return view;
	}

	@SuppressLint("NewApi")
	private void setUpGroupIndicator() {
		DisplayMetrics metrics;
		int width;
		metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay()
				.getMetrics(metrics);
		width = metrics.widthPixels;
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			expandableListView.setIndicatorBounds(width - GetPixelFromDips(35),
					width - GetPixelFromDips(5));

		} else {
			expandableListView.setIndicatorBoundsRelative(width
					- GetPixelFromDips(35), width - GetPixelFromDips(5));
		}
	}

	private int GetPixelFromDips(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	public class MyExpandableAdapter extends BaseExpandableListAdapter {

		private Context context;
		private ArrayList<Object> childItems;
		private ArrayList<String> parentItems;

		// constructor
		public MyExpandableAdapter(Context context, ArrayList<String> parents,
				ArrayList<Object> childItems) {
			this.context = context;
			this.parentItems = parents;
			this.childItems = childItems;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Object getChild(int groupPosition, int childPosition) {
			ArrayList<String> child = (ArrayList<String>) childItems
					.get(groupPosition);
			return child.get(groupPosition);

		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {

			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.layout_expandable_child_item, null);
			}
			@SuppressWarnings("unchecked")
			ArrayList<String> child = (ArrayList<String>) childItems
					.get(groupPosition);
			TextView txt = (TextView) convertView
					.findViewById(R.id.text_child_id);

			txt.setText(child.get(childPosition));

			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {

			@SuppressWarnings("unchecked")
			ArrayList<String> child = (ArrayList<String>) childItems
					.get(groupPosition);
			return child.size();
		}

		@Override
		public Object getGroup(int groupPosition) {

			return parentItems.get(groupPosition);
		}

		@Override
		public int getGroupCount() {

			return parentItems.size();
		}

		@Override
		public long getGroupId(int groupPosition) {

			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.layout_expandable_group_item_bg, null);
			}

			TextView txt = (TextView) convertView
					.findViewById(R.id.text_group_id);
			txt.setText(parentItems.get(groupPosition));

			ExpandableListView mExpandableListView = (ExpandableListView) parent;
			mExpandableListView.expandGroup(groupPosition);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {

			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {

			return false;
		}
	}

	private void setChildData() {
		// Add Child Items for Fruits
		childItems = new ArrayList<Object>();

		ArrayList<String> child = new ArrayList<String>();
		child.add(restaurant.getCuisine());

		childItems.add(child);

		// Add Child Items for Flowers
		child = new ArrayList<String>();
		child.add(!restaurant.getOfferAvailable().equals("No Offers") ? restaurant
				.getOfferAvailable().equals("31") ? "2 for 1 meal"
				: "50% off food" : restaurant.getOfferAvailable());

		childItems.add(child);

		// Add Child Items for Animals
		child = new ArrayList<String>();
		child.add(restaurant.getPeople());

		childItems.add(child);

		// Add Child Items for Birds
		child = new ArrayList<String>();
		child.add(restaurant.getDescription());

		childItems.add(child);

	}

	private void setGroupParents() {
		parentItems = new ArrayList<String>();

		parentItems.add("CUISINE");
		parentItems.add("OFFER TYPE");
		parentItems.add("NO OF PEOPLE");
		parentItems.add("RESTAURANT INFORMATION");
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.text_web_url:

			String url = webUrl.getText().toString();

			if (!url.startsWith("http://") && !url.startsWith("https://"))
				url = "http://" + url;
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(url));
			startActivity(browserIntent);

			break;
		case R.id.text_phone_num:

			String uri2 = "tel:" + phoneNumber.getText().toString().trim();
			Intent intent2 = new Intent(Intent.ACTION_CALL);
			intent2.setData(Uri.parse(uri2));
			startActivity(intent2);
			break;

		default:
			break;
		}

	}
}
